using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class LostUI : MonoBehaviour
{
    private VisualElement root;
    private Button mainMenuButton;

    // Start is called before the first frame update
    void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        mainMenuButton = root.Q<Button>("mainmenu-button");

        mainMenuButton.clicked += MainMenuButtonPressed;

        GameManager._instance.OnGameDead.AddListener(OnDeadReceived);
        GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
        OnResumedReceived();
    }

    private void OnDestroy()
    {
        mainMenuButton.clicked -= MainMenuButtonPressed;
    }

    

    private void MainMenuButtonPressed()
    {
        Debug.Log("Go to main menu!");
        GameManager._instance.ResumeGame();
        root.style.visibility = Visibility.Hidden;
        SceneManager.LoadScene("MainMenu");
        
    }

    private void OnDeadReceived()
    {
        root.style.visibility = Visibility.Visible;
    }

    private void OnResumedReceived()
    {
        root.style.visibility = Visibility.Hidden;
    }
}
