using System.Collections;
using UnityEngine;

public class EnemyMovementTimer : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float timer; // Total time for each movement
    private bool isMovingRight = true; // Start by moving to the right
    private float timerElapsed = 0f;
    [SerializeField] private SpriteRenderer spriteRenderer;
    private Vector3 direction;

    private bool isWaiting = false; // Flag to control waiting
    private float waitTimer = 0f; // Timer for waiting

    private void Start()
    {
        direction = isMovingRight ? Vector3.right : Vector3.left;
        // Start moving immediately
        StartMoving();
    }

    private void Update()
    {
        if (isWaiting)
        {
            waitTimer += Time.deltaTime;

            if (waitTimer >= 2f)
            {
                isMovingRight = !isMovingRight; // Change direction
                StartMoving();
                isWaiting = false; // Stop waiting
                waitTimer = 0f; // Reset the wait timer
            }
        }
        else
        {
            timerElapsed += Time.deltaTime;

            // Check if the timer duration has elapsed
            if (timerElapsed >= timer)
            {
                isWaiting = true; // Start waiting
                timerElapsed = 0f; // Reset the timer
            }

            // Move smoothly in the current direction
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }

    private void StartMoving()
    {
        direction = isMovingRight ? Vector3.right : Vector3.left;
        FlipSprite(direction);
    }

    private void FlipSprite(Vector3 direction)
    {
        spriteRenderer.flipX = direction.x < 0f;
    }
}
