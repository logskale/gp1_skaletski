using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    private HUDController HUD;
    private AudioSource source;
    [SerializeField] private AudioSource hitSoundClip;


    private void Start()
    {
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();
        source = GetComponent<AudioSource>(); // Get the AudioSource component from the same GameObject as this script.
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            Debug.Log("You have been hurt");
            GameManager._instance.SubtractLives(1);
            HUD.Hit();
            GameManager._instance.DeadGame();

            // Play the hit sound
            if (source != null && hitSoundClip != null)
            {
                hitSoundClip.Play();
                
            }
        }
    }
}
