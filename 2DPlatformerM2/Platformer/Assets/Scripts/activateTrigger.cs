using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activateTrigger : MonoBehaviour
{
    public GameObject appearingBlock; // Reference to the GameObject to activate.

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the entering object has the "Statue" tag.
        if (other.CompareTag("Statue"))
        {
            // Activate the appearingBlock GameObject.
            if (appearingBlock != null)
            {
                appearingBlock.SetActive(true);
            }
        }
    }
}
