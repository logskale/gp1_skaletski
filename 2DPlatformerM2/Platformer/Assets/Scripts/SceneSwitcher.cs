using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    private string sceneNameToLoad = "Level2"; // Change this to the name of your Level2 scene.

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the object that entered the trigger is the player (you might need to change the tag or check for a specific player object in a different way).
        if (other.CompareTag("Player"))
        {
            // Load the specified scene.
            SceneManager.LoadScene(sceneNameToLoad);
        }
    }
}
