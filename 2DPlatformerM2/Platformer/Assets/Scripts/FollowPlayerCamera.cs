using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowCamera : MonoBehaviour
{
    [SerializeField] private Transform followTarget;
    [SerializeField] bool directFollow = true;
    [SerializeField] private float cameraDistance = -10f;
    [SerializeField] private float smoothFollowSpeed = 0.2f;
    

    [SerializeField] bool onlyMoveRight = false;
    float minimumXPosition = 0f;
    private Camera thisCamera;

    private void Start()
    {
        // Initializing our minimum x position to that of the followTarget
        minimumXPosition = followTarget.position.x;
        
    }

    private void LateUpdate()
    {
        // If the followtarget is further right, update the minimum x position
        if (followTarget.position.x > minimumXPosition)
        {
            minimumXPosition = followTarget.position.x;
        }

        // Direct camera follow
        if (directFollow)
        {
            Vector3 directTargetPosition = followTarget.position;
            directTargetPosition.z = cameraDistance;

            // Move our camera's position to that of the followTarget, but with -10 for z
            transform.position = directTargetPosition;
        }
        // Smooth camera follow
        else
        {
            Vector3 currentPosition = transform.position; // Where are we now
            currentPosition.z = followTarget.position.z; // Get rid of the z component for the sake of calculations

            // Lerp from where we are towards where the followTarget is
            Vector3 smoothTargetPosition = Vector3.Lerp(currentPosition, followTarget.position, smoothFollowSpeed);
            smoothTargetPosition.z = cameraDistance; // Move the camera back so everything is rendered

            // If we're only moving right
            if (onlyMoveRight)
            {
                // Prevent our camera from moving left
                smoothTargetPosition.x = minimumXPosition;
            }

            // Move our camera's position to the calculated smooth position value
            transform.position = smoothTargetPosition;
        }
    }
}
