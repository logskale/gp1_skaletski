using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HUDController : MonoBehaviour
{

    [SerializeField] private UIDocument UIDoc;
    [SerializeField] private Sprite heartImage;
    private VisualElement heartsContainer;
    private static bool created = false;

    private void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(gameObject);
            created = true;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager._instance.lives = 0;
        VisualElement root = UIDoc.rootVisualElement;
        heartsContainer = root.Q<VisualElement>("Lives");


    }


    public void AddLife(VisualElement container)
    {
        Image heart = new Image();
        heart.sprite = heartImage;

        heart.style.paddingTop = 5;
        heart.style.paddingLeft = 0;
        heart.style.paddingRight = 0;

        heart.style.width = 32;
        heart.style.height = 32;

        heart.style.flexGrow = 0;
        heart.style.flexShrink = 0;

        container.Add(heart);
    }
    public void RemoveLife(VisualElement container)
    {
        int heartCount = container.childCount;

        if (heartCount > 0)
        {
            GameManager._instance.lives -= 5;
        }
    }
    public void Hit()
    {
        RemoveLife(heartsContainer);
    }
    public void Gain()
    {
        AddLife(heartsContainer);
    }
    
}
