using UnityEngine;

public class EmissionControl : MonoBehaviour
{
    public float frequency = 2.0f;  // Controls the speed of the sine wave
    public float amplitude = 1.0f;  // Controls the amplitude of the sine wave
    public float hdrIntensity = 1.0f; // Controls the HDR intensity

    private Material material;
    private Color baseColor;

    void Start()
    {
        material = GetComponent<Renderer>().material;
        baseColor = material.GetColor("_EmissionColor"); // "_EmissionColor" is the name of the emission color property in the shader
    }

    void Update()
    {
        // Calculate the new emission intensity using a sine wave
        float emissionIntensity = Mathf.Sin(Time.time * frequency) * amplitude;

        // Calculate the new HDR intensity
        float newHDRIntensity = hdrIntensity + emissionIntensity;

        // Update the material's emission color with the new HDR intensity
        Color newEmissionColor = baseColor * newHDRIntensity;
        material.SetColor("_EmissionColor", newEmissionColor);
    }
}
