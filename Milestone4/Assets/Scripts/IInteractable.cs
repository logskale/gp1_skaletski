using UnityEngine;

/// <summary>
/// This is the IInteractable interface, which scripts can inherit
/// from in order to signify that they can be interacted with.
/// </summary>

public interface IInteractable
{
    /// <summary>
    /// This function will execute the functionality to interact for this particular interactable object.
    /// Include the PlayerInteractManager and PlayerBody as parameters so interactables can use their references.
    /// </summary>
    /// <param name="playerInteractManager">The PlayerInteractManager that interacted with this IInteractable.</param>
    /// <param name="playerBody">The PlayerBody that owns the PlayerInteractManager that interacted with this IInteractable.</param>
    void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody);
}
