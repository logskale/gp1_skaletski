//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[CreateAssetMenu(menuName = "InteractBehaviours/PrintMessage", fileName = "PrintMessageIBSO", order = 0)]
//public class PrintMessageIBSO : InteractBehaviourSO
//{
//    // The message to print upon being interacted with
//    [SerializeField] private string messageToPrint = "DefaultMessage";

//    public override void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
//    {
//        // When interacted with, print the message
//        Debug.Log(messageToPrint);
//    }
//}
