using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

/// <summary>
/// This is the PlayerBody class, a specific child class of
/// the CharacterBody class. This script is meant to handle
/// special movement types for the player, and also handle
/// camera-related functionality.
/// </summary>

public class PlayerBody : CharacterBody
{
    #region Variables

    [Space(20)]

    [Header("Player - Ground Movement")]
    [SerializeField] private float moveAcceleration = 60f; // The speed at which this character accelerates in m/s
    [SerializeField] private float walkSpeed = 4f; // The max horizontal speed of this character (when walking) in m/s
    [SerializeField] private float sprintSpeed = 7f; // The max horizontal speed of this character (when running) in m/s
    private float maxVerticalMoveSpeed = 25f; // The maximum vertical move speed of this character
    private bool isSprinting = false; // Indicates whether you are sprinting or not

    [Header("Player - Air Movement")]
    [SerializeField] private int maxJumps = 2; // The maximum number of times this character can jump before returning to the ground
    private int currentJump = 0; // The number of times you have jumped since leaving the ground
    [SerializeField] private float jumpCooldown = 0.25f; // The minimum amount of time that must elapse between jumps
    [SerializeField] private float airControlMultiplier = 0.4f; // The multiplier used to affect the amount of control you have in the air
    private bool readyToJump = true; // Boolean flag indicating if our jump cooldown is over

    [Header("Player - Rotation")]
    [SerializeField] private float playerGroundRotationSpeed = 10f; // The speed at which the player rotates (when grounded)
    [SerializeField] private float playerAirRotationSpeed = 3f; // The speed at which the player rotates (when airborne)

    [Header("Player - Ground Check")]
    [SerializeField] private float groundCheckDistance = 0.1f; // The distance below the player to check for ground
    [SerializeField] LayerMask environmentLayerMask; // Which layers are considered to be the environment
    private bool wasGroundedLastFrame; // Denotes whether you were on the ground last frame or not
    private bool isGrounded; // Denotes whether you are on the ground or not

    [Header("Player Input")]
    private Vector2 movementInput; // The Movement Input from our player

    [SerializeField] private float maxSlopeAngle = 44f;
    [SerializeField] private float slopeStickingForce = 2f;
    private RaycastHit slopeHit;
    private bool isOnSlope;
    private bool exitingSlope;

    [Header("State Handling")]
    private MovementState movementState=MovementState.Walking;
    public enum MovementState {
        Walking,
        Sprinting,
        Airborne
    }

    [Header("Camera")]
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Transform cameraOrientation;
    [SerializeField] private PlayerInteractManager playerInteractManager;

    #endregion

    #region Unity Functions

    private void FixedUpdate()
    {
        // Every frame, check if we are on the ground.
        CheckGrounded();

        // Move our character each frame.
        MoveCharacter();

        // Every physics update, make sure that
        // we are not exceeding our current
        // maximum allowed velocity.
        LimitVelocity();
    }

    private void Update()
    {
        // Every frame we should recalculate our
        // camera relative inputs since the camera
        // can move at any time.
        CalculateCameraRelativeInput();

        StateHandler();

        // Rotate our character to face towards our input.
        RotateCharacter();


        // Here we can get the velocity of our rigidbody
        // and use that to determine if we are moving or
        // not. Then we can use that information to tell
        // our animation if we should be moving or idle.
        // We only want velocity in the horizontal xz dimension,
        // because that's what tells us if we're running.
        // We take the magnitude of the xz portion of our velocity,
        // because we don't care about direction, just
        // the magnitude of the speed.
        animator.SetFloat("HorizontalSpeed", GetHorizontalRBVelocity().magnitude);

        // We can also tell if we're going up or down using
        // rigidbody velocity. Because we have an animation
        // for going up and down separately in the air, this
        // is important to know.
        animator.SetBool("IsFalling", (characterRigidbody.velocity.y < 0f));
    }

    #endregion

    #region Custom Functions

    #region Input Processing

    /// <summary>
    /// This function will calculate our movement input relative
    /// to our camera's orientation. If we do not do this, our input
    /// will be relative to world coordinates, and will not be intuitive
    /// to use. This makes it so that if you input movement to the right,
    /// rather than "global right" (1f,0f), it will point to the right
    /// of whatever direction the camera is facing in.
    /// </summary>
    void CalculateCameraRelativeInput()
    {
        // Do nothing if there is no movement input.
        // No need to waste CPU time on pointless calculations.
        if (movementInput == Vector2.zero)
        {
            movementDirection = Vector3.zero;
            return;
        }

        // Calculate the direction that we should move
        // in by adjusting our movement input to match
        // our camera orientation. We do this by using
        // our cameraOrientation object to get our relative
        // forward and right vectors, then we scale those
        // by the magnitude of our input and add them together.
        movementDirection = cameraOrientation.forward * movementInput.y + cameraOrientation.right * movementInput.x;

        // If our input has a magnitude of greater than 1, we
        // should normalize our input direction vector.
        // We only want to do this if the value is greater than
        // 1, since smaller values are desirable if we aren't
        // performing a full input, such as with a joystick
        // partially moved in a direction.
        if (movementDirection.sqrMagnitude > 1f)
        {
            movementDirection = movementDirection.normalized;
        }
    }

    #region Input Actions

    public override void SetMoveInput(Vector2 moveInput)
    {
        // Read in the Vector2 of our player input.
        movementInput = moveInput;

        // Re-calculate our camera relative
        // input, as our input has changed.
        CalculateCameraRelativeInput();
    }

    #endregion

    #endregion

    #region Movement

    protected override void MoveCharacter()
    {
        // We only need to apply forces to move
        // if we are trying to move. Thus, if we
        // aren't inputting anything, don't apply
        // any forces (they'd be 0 anyways).
        if (movementDirection != Vector3.zero)
        {
            if (isOnSlope)
            {
                Vector3 slopeAdjustedMovementDirection = Vector3.ProjectOnPlane(movementDirection, slopeHit.normal).normalized;
                characterRigidbody.AddForce(slopeAdjustedMovementDirection * moveAcceleration * characterRigidbody.mass, ForceMode.Force);
                if(!Mathf.Approximately(characterRigidbody.velocity.y, 0f) && !exitingSlope)
                {
                    characterRigidbody.AddForce(transform.up * -1f * slopeStickingForce * characterRigidbody.mass, ForceMode.Force);
                }
            }
            // If we are on the ground we want
            // to move according to our movespeed.
            else if (isGrounded)
            {
                // Apply our movement Force.
                characterRigidbody.AddForce(movementDirection * moveAcceleration * characterRigidbody.mass, ForceMode.Force);
            }
            // Otherwise, if we are in the air we want to
            // move according to our movespeed modified by
            // our airControlMultiplier.
            else //Is in air
            {
                // Apply our movement force multiplied by
                // our airControlMultiplier.
                characterRigidbody.AddForce(movementDirection * moveAcceleration * airControlMultiplier * characterRigidbody.mass, ForceMode.Force);
            }
        }
        characterRigidbody.useGravity = !isOnSlope;
    }

    protected override void RotateCharacter()
    {
        // Rotate our camera orientation based on the position
        // of our camera object relative to the position of
        // our player object.
        Vector3 basicViewDir = transform.position - cameraTransform.position;
        basicViewDir.y = 0f;
        cameraOrientation.forward = basicViewDir.normalized;

        // If the player is making an input, rotate the player
        // object to face our input direction.
        if (movementDirection != Vector3.zero)
        {
            characterModel.forward = Vector3.Slerp(characterModel.forward, movementDirection.normalized, Time.deltaTime * rotationSpeed);
        }
    }

    /// <summary>
    /// This function is called in every FixedUpdate call.
    /// This will ensure that if we are moving faster than
    /// our maximum allowed velocity, we will slow down to
    /// that maximum velocity.
    /// </summary>
    private void LimitVelocity()
    {

        // Use only our horizontal velocity
        Vector3 currentVelocity = Vector3.zero;
        if (isOnSlope && !exitingSlope)
        {
            currentVelocity = characterRigidbody.velocity;
        }
        else
        {
            currentVelocity = GetHorizontalRBVelocity();
        }
        // If our current velocity is greater than
        // our maximum allowed velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();
        if (currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            // Use an impulse force to counteract our
            // velocity to slow down to max allowed velocity.
            Vector3 counteractDirection = currentVelocity.normalized * -1f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;
            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        // If we are in the air, we should
        // constrain our vertical velocity.
        if (!isGrounded)
        {
            // If our current vertical velocity is
            // greater than our maximum vertical velocity.
            if(Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                // Use an impulse force to counteract our vertical
                // velocity to slow down to maxVerticalMoveSpeed.
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;
                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;
                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }

    }

    /// <summary>
    /// This is a helper function that calculates the current
    /// maximum allowed movement velocity based on moveSpeed and
    /// the magnitude of our player movement input.
    /// </summary>
    /// <returns></returns>
    public float GetMaxAllowedVelocity()
    {
        return moveSpeed * movementDirection.magnitude;
    }

    /// <summary>
    /// This function is called when our jump action
    /// is performed. It will launch the character
    /// upwards (if we are able to jump) based on our jumpForce.
    /// </summary>
    public override void Jump()
    {

        // If we're ready to jump (cooldown) and we're either on the ground or still have more jumps we can perform
        if (readyToJump && (isGrounded || currentJump < maxJumps))
        {
            exitingSlope = true;
            // Increment and track our current jump
            currentJump += 1;
            // Zero out any vertical velocity, so we can have a consistent jump height every time
            Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;
            float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y);
            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            // Jump
            characterRigidbody.AddForce(Vector3.up * jumpForce * characterRigidbody.mass, ForceMode.Impulse);
            //Start our jump cooldown
            readyToJump = false;
            StartCoroutine(JumpCooldownCoroutine());
        }
    
    }
    private IEnumerator JumpCooldownCoroutine()
    {
        yield return new WaitForSeconds(jumpCooldown);
        readyToJump = true;

        exitingSlope = false;
    }

    /// <summary>
    /// This function is called jumpCooldown seconds after
    /// we jump. This function resets our ability to jump.
    /// </summary>
    private void HandleJumpCooldown()
    {
        // Indicate that we can jump again
        readyToJump = true;
    }

    /// <summary>
    /// This function is called when our jump action
    /// is canceled. If we are still heading upwards,
    /// we will halve our vertical velocity. This enables
    /// us to perform partial jumps.
    /// </summary>
    public override void JumpCanceled()
    {
        // If we are heading upwards
        if (characterRigidbody.velocity.y > 0)
        {
            // Halve our vertical velocity
            characterRigidbody.AddForce(Vector3.down * (characterRigidbody.velocity.y / 2f) * characterRigidbody.mass, ForceMode.Impulse);
        }
    }

    /// <summary>
    /// Tell the PlayerBody to begin sprinting.
    /// </summary>
    public void StartSprinting()
    {
        isSprinting = true;
       
    }

    /// <summary>
    /// Tell the PlayerBody to end sprinting.
    /// </summary>
    public void StopSprinting()
    {
        isSprinting = false;
        
    }

    #endregion
    #region State Hangling

    private void StateHandler()
    {
        MovementState targetState = MovementState.Walking;

        if (!isGrounded)
        {
            targetState = MovementState.Airborne;
        }
        else if (isSprinting)
        {
            targetState = MovementState.Sprinting;
        }
        else
        {
            targetState = MovementState.Walking;
        }

        if (targetState == movementState) return;

        movementState = targetState;
        switch (movementState)
        {
            case MovementState.Walking:
                moveSpeed = walkSpeed;
                rotationSpeed = playerGroundRotationSpeed;
                break;
            case MovementState.Sprinting:
                moveSpeed = sprintSpeed;
                rotationSpeed = playerGroundRotationSpeed;
                break;
            case MovementState.Airborne:
                rotationSpeed = playerAirRotationSpeed;
                break;
            default:
                Debug.LogError("movement state error");
                break;


        }
    }

    #endregion
    /// <summary>
    /// This function will be called every physics
    /// frame in FixedUpdate and will determine if
    /// the character is on the ground or not.
    /// </summary>
    private void CheckGrounded()
    {
        // Let's store whether or not we were grounded
        // last frame so we can know if this is the
        // frame where we became grounded
        wasGroundedLastFrame = isGrounded;

        // Here we will use an overlapSphere to examine a spherical
        // area under our player and check if there are any
        // objects in the Environment layer there.
        // Based on whether or not we found a collider with
        // our overlapSphere, we will know if we are on the ground.
        Vector3 overlapSphereOrigin = transform.position + (Vector3.up * (capsuleCollider.radius - groundCheckDistance));
        Collider[] overlappedColliders = Physics.OverlapSphere(overlapSphereOrigin, capsuleCollider.radius * 0.95f, environmentLayerMask, QueryTriggerInteraction.Ignore);

        // We are grounded if there is at least one ground
        // layer collider right beneath us
        isGrounded = (overlappedColliders.Length > 0);

        // If we were not grounded last frame but we are this
        // frame, then that means we became grounded this frame.
        if (!wasGroundedLastFrame && isGrounded)
        {
            // Reset jumps when we hit the ground
            currentJump = 0;
        }
        // If we were grounded last frame but we are not
        // this frame, then that means we became airborned this frame.
        else if(wasGroundedLastFrame && !isGrounded)
        {
            // If we left the ground without jumping,
            // increment our jump counter.
            if(currentJump == 0)
            {
                currentJump = 1;
            }
        }

        CheckSlope();

        // Also, don't forget to let the animator know whether
        // or not we're grounded so we can play the right animation.
        animator.SetBool("IsGrounded", isGrounded);
    }

    private void CheckSlope()
    {
        if (isGrounded)
        {
            isOnSlope = false;
            return;
        }

        //Raycast downwards to find the ground
        //If we found the ground
        if(Physics.Raycast(transform.position+ (Vector3.up*.1f), transform.up*-1f,out slopeHit, groundCheckDistance+0.1f, environmentLayerMask))
        {
            float slopeAngle = Vector3.Angle(Vector3.up, slopeHit.normal);
            isOnSlope = (slopeAngle < maxSlopeAngle) && (slopeAngle >= 0.1f);
        }
        else
        {
            isOnSlope = false;
        }
        
    }

    #region Helper Functions

    /// <summary>
    /// This is a helper function that will strip the y component out of
    /// our Rigidbody velocity and give us back a vector with just the
    /// x and z (horizontal) components of our velocity.
    /// </summary>
    /// <returns>The XZ (horizontal) velocity Vector3.</returns>
    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0f, characterRigidbody.velocity.z);
    }

    #endregion
    public void Interact()
    {
        playerInteractManager.Interact();
    }
    #region Debug
    private void OnDrawGizmos()
        {
            // Show ground check
            Gizmos.color = isGrounded ? Color.green : Color.red;
            Vector3 overlapSphereOrigin = transform.position + (Vector3.up * (capsuleCollider.radius - groundCheckDistance));
            Gizmos.DrawWireSphere(overlapSphereOrigin, capsuleCollider.radius * 0.95f);
        }

    #endregion
    #endregion






}
