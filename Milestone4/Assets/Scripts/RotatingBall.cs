using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This is an example script to show proper usage of the IInteractable interface.
/// You can add such an interface even to a script that does things other than handle interaction.
/// For example, this script rotates the ball over time, but we can also handle interaction here.
/// </summary>
public enum InteractionType
{
    PlaySound,
    PrintMessage,
    TranslateObject,
    RotateObject,
    ChangeScene,
    DestroyObject,
    WonGame// For moving the object
}
public class RotatingBall : MonoBehaviour, IInteractable
{
    [SerializeField] private float rotateSpeed = 60f;
    [SerializeField] private Material swapMaterial;
    [SerializeField] private MeshRenderer meshRenderer;

    [SerializeField] private InteractionType activeInteraction = InteractionType.PlaySound;

    [SerializeField] private AudioClip audioClip;
    [SerializeField] private AudioSource audioSource;
    private bool isRotating;

    public void SetActiveInteraction(InteractionType interactionType)
    {
        activeInteraction = interactionType;
    }

    void Update()
    {
        // Rotate the GameObject that this script is on
        if (isRotating)transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);    
    }

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        // Determine the action based on the activeInteraction selected by the designer
        switch (activeInteraction)
        {
            case InteractionType.PlaySound:
                // Play sound logic here
                // Replace 'YourSoundMethod()' with your actual sound-playing logic
                YourSoundMethod();
                break;

            case InteractionType.PrintMessage:
                // Print message logic here
                Debug.Log("Eww, Don't touch me");
                break;

            case InteractionType.TranslateObject:
                // Translate object logic here
                transform.Translate(Vector3.down*10); // Translate 1 meter in the x direction
                break;

            case InteractionType.RotateObject:
                // Translate object logic here
                isRotating = !isRotating;
                break;

            case InteractionType.ChangeScene:
                // Translate object logic here
                SceneManager.LoadSceneAsync(2);
                break;

            case InteractionType.DestroyObject:
                YourSoundMethod();
                // Tell the PlayerInteractManager to stop tracking this object
                playerInteractManager.StopTrackingObject(gameObject);

                // Destroy the GameObject this script is on when interacted with
                Destroy(gameObject);
                break;
            case InteractionType.WonGame:
                // Translate object logic here
                SceneManager.LoadSceneAsync(3);
                break;

            default:
                Debug.LogWarning("Invalid interaction type selected.");
                break;
        }
    }

    // Replace this method with your actual sound-playing logic using Unity's audio system
    private void YourSoundMethod()
    {
        audioSource.PlayOneShot(audioClip);
    }
    
}

