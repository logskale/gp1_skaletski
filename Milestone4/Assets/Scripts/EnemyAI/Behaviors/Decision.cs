using UnityEngine;

/// <summary>
/// The Decision class represents some condition that is assessed to be either true or false and is used to execute State Transitions.
/// </summary>

public abstract class Decision : ScriptableObject
{
    // A function that is overridden by child classes to determine some condition
    public abstract bool Decide(Blackboard blackboard);
}