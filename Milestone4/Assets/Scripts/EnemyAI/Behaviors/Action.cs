using UnityEngine;

/// <summary>
/// The Action class represents some piece of functionality performed in some State of the FSM.
/// </summary>

public abstract class Action : ScriptableObject
{
    // A function that is overridden by child classes to perform a particular action
    public abstract void Act(Blackboard blackboard);
}