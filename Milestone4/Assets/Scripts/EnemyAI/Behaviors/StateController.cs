using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshAgent))]
public class StateController : MonoBehaviour
{
    // the context that our controller is currently in
    public State currentState;

    public bool aiActive = true;

    [SerializeField] private NavMeshAgent navMeshAgent;

    //indicates how long the AI has been the current state
    public float stateTimeElapsed = 0f;

    [Tooltip("A timer value that indicates how long it has been since the AI last performed a timed action")]
    public float actionTimeElapsed;

    public Blackboard blackboard;

    #region Unity Functions

    private void Start()
    {
        TransitionToState(currentState);
    }

    private void Update()
    {
        if (!aiActive) return;

        IncrementTimers();

        currentState.UpdateState(blackboard);
    }

    private void Awake()
    {
        Setup();
    }

    #endregion

    #region Custom Functions
    /// <summary>
    /// Transition the context of the StateController from the currentState to nextState
    /// </summary>
    /// <param name="nextState">The state to transition to</param>
    /// <returns>A bool indicating if the transition was successful</returns>
    public bool TransitionToState(State nextState)
    {
        //only want to transition if the nextState is not null
        if (nextState != null)
        {
            //perform valuse cleanup for the current context before exit
            onExitState();

            //change to the new state
            Debug.Log($"transitioning to state: {nextState}");
            currentState = nextState;

            //entering to new state
            currentState.EnterState(blackboard);

            //transition succeeded
            return true;
        }
        else
        {
            //Transition failed
            return false;
        }
    }

    private void Setup()
    {
        //dynamically create our blackboard
        //blackboard = new Blackboard(this);
    }

    private void onExitState()
    {
        currentState.ExitState(blackboard);

        stateTimeElapsed = 0f;
        actionTimeElapsed = 0f;
    }

    private void IncrementTimers()
    {
        stateTimeElapsed += Time.deltaTime;
        actionTimeElapsed += Time.deltaTime;
    }

    public bool CheckIfStateTimeElapsed(float duration)
    {
        return (stateTimeElapsed >= duration);
    }

    public bool CheckIfActionTimeElapsed(float duration)
    {
        bool result = (actionTimeElapsed >= duration);
        if (result)
        {
            // Reset our action timer when the time elapsed has exceeded the duration given
            actionTimeElapsed = 0f;
        }

        return result;
    }

    #endregion
    #region Debug

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR

        GUIStyle handlesStyle = new GUIStyle();
        handlesStyle.normal.textColor = Color.red;
        handlesStyle.fontSize = 24;
        UnityEditor.Handles.Label(transform.position, currentState.name, handlesStyle);
#endif
    }

    #endregion
}
