using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="PlugableAI/Decisions/WaitForTime", fileName ="D_WaitForTIme")]
public class StateTimeDecision : Decision
{
    [SerializeField] private float waitForTime = 3f;
    public override bool Decide(Blackboard blackboard)
    {
        bool waitForTimeReached = blackboard.owningController.CheckIfStateTimeElapsed(waitForTime);
        return waitForTimeReached;

    }
}
