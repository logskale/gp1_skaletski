using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ChaseTargetInRangeDecision evaluates to true if
/// the AI agent is close enough to their chaseTarget
/// based on their lookRange and false otherwise.
/// See AgentDistanceDecision for a more general version.
/// </summary>

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ChaseTargetInRange", fileName = "D_ChaseTargetInRange")]
public class ChaseTargetInRangeDecision : Decision
{
    public override bool Decide(Blackboard blackboard)
    {
        // Do nothing if the chaseTarget is null
        if (blackboard.chaseTarget == null) return false;

        bool chaseTargetInRange = ChaseTargetInRange(blackboard);

        return chaseTargetInRange;
    }

    private bool ChaseTargetInRange(Blackboard blackboard)
    {
        // Get the vector from the AI to the chase target
        Vector3 vectorToChaseTarget = (blackboard.chaseTarget.position - blackboard.owningController.transform.position);

        // Draw a ray in the direction of the chase target with a distance of look range
        Debug.DrawRay(blackboard.eyes.position, vectorToChaseTarget.normalized * blackboard.aiStats.lookRange, Color.yellow);

        // Calculate the squared distance to the target
        float sqrDistanceToChaseTarget = vectorToChaseTarget.sqrMagnitude;

        // Compare the square distance to the target to the squared look range
        bool chaseTargetInRange = (sqrDistanceToChaseTarget <= (blackboard.aiStats.lookRange * blackboard.aiStats.lookRange));

        // Return the result
        return chaseTargetInRange;
    }
}