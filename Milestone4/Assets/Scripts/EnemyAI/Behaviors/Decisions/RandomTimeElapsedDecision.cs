using UnityEngine;

[CreateAssetMenu(menuName = "Decision/RandomTimeElapsed", fileName = "D_RandomTimeElapsedDecision")]
public class RandomTimeElapsedDecision : Decision
{
    public float minTime = 2f;
    public float maxTime = 5f;

    public override bool Decide(Blackboard blackboard)
    {
        float elapsedTime = blackboard.owningController.stateTimeElapsed;
        float randomTime = Random.Range(minTime, maxTime);

        return elapsedTime >= randomTime;
    }
}
