using UnityEngine;
[CreateAssetMenu(menuName = "Decision/Not", fileName = "D_NotDecision")]
public class NotDecision : Decision
{
    public Decision decision;

    public override bool Decide(Blackboard blackboard)
    {
        return !decision.Decide(blackboard);
    }
}
