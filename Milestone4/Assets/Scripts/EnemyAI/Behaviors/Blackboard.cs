using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// The Blackboard class represents all information
/// that may be needed by the Finite State Machine.
/// </summary>

// This allows this class to be serialized by Unity as an asset
[System.Serializable]
public class Blackboard
{
    [Tooltip("The StateController that owns this Blackboard.")]
    public StateController owningController;

    [Tooltip("The NavMeshAgent of the owning SateController.")]
    public NavMeshAgent navMeshAgent;

    [Tooltip("A ScriptableObject that contains certian information about certain AI operations (like navigations)")]
    public AIStats aiStats;

    [Tooltip("The home waypoint of the AI.")]
    public Transform homeWaypoint;

    [Tooltip("The position from which the AI performs visual perception operations like LookDecision")]
    public Transform eyes;

    [Tooltip("The Transform of the object the AI is navigating towards")]
    public Transform chaseTarget;

    [Tooltip("A list of waypoints for the AI to patrol around")]
    public List<Transform> waypointList;

    [Tooltip("The index of the next waypoint to travel to")]
    public int nextWaypoint;

    public Blackboard()
    {
        this.owningController = null;
    }

    public Blackboard(StateController owningController)
    {
        this.owningController = owningController;
    }
}