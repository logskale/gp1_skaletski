using UnityEngine;

/// <summary>
/// The PatrolAction makes the AI agent patrol along the path of the waypoints defined in the StateController.
/// </summary>

[CreateAssetMenu(menuName = "PlugableAI/Actions/Patrol", fileName = "A_Patrol")]
public class PatrolAction : Action
{
    public override void Act(Blackboard blackboard)
    {
        Patrol(blackboard);
    }

    private void Patrol(Blackboard blackboard)
    {
        blackboard.navMeshAgent.destination = blackboard.waypointList[blackboard.nextWaypoint].position;
        blackboard.navMeshAgent.isStopped = false;

        // If our agent is ready at their destination and is not calculating a path
        if (blackboard.navMeshAgent.remainingDistance <= blackboard.navMeshAgent.stoppingDistance && !blackboard.navMeshAgent.pathPending)
        {
            // Set next waypoint
            blackboard.nextWaypoint = (blackboard.nextWaypoint + 1) % blackboard.waypointList.Count;
        }
    }
}