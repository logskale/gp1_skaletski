using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// The AttackAction makes the AI agent perform an
/// attack when they are close enough to the player.
/// </summary>

[CreateAssetMenu(menuName = "PlugableAI/Actions/Attack", fileName = "A_Attack")]
public class AttackAction : Action
{
    
    [SerializeField] private LayerMask characterLayerMask;

    public override void Act(Blackboard blackboard)
    {
        Attack(blackboard);
    }

    private void Attack(Blackboard blackboard)
    {
        Debug.DrawRay(blackboard.eyes.position, blackboard.eyes.forward.normalized * blackboard.aiStats.attackRange, Color.red);

        // If our spherecast hit something and the
        // thing that it hit is the player
        RaycastHit hit;
        Collider[] cols;
        if (Physics.SphereCast(blackboard.eyes.position, blackboard.aiStats.lookSphereCastRadius, blackboard.eyes.forward, out hit, blackboard.aiStats.attackRange, characterLayerMask, QueryTriggerInteraction.Ignore)
            && hit.collider.CompareTag("Player"))
        {
            // If our AI is able to attack (countdown), and the player is within range
            if (blackboard.owningController.CheckIfActionTimeElapsed(blackboard.aiStats.attackRate))
            {
                Debug.Log("AI " + blackboard.owningController.gameObject.name + " Attacks!");
                GameManager._instance.SubtractLives(1);
                if (GameManager._instance.lives <= 0)
                {
                    SceneManager.LoadSceneAsync(4);
                }
            }
        }
        // Do a secondary check with an OverlapSphere to check the initial position of the spherecast.
        // Note: SphereCast will not detect colliders for which the sphere initially overlaps the collider.
        else if ((cols = Physics.OverlapSphere(blackboard.eyes.position, blackboard.aiStats.lookSphereCastRadius, characterLayerMask, QueryTriggerInteraction.Ignore)) != null)
        {
            // For each overlapped collider
            foreach (Collider collider in cols)
            {
                // If this is the Player
                if (collider.CompareTag("Player"))
                {
                    // If our AI is able to attack (countdown), and the player is within range
                    if (blackboard.owningController.CheckIfActionTimeElapsed(blackboard.aiStats.attackRate))
                    {
                        Debug.Log("AI " + blackboard.owningController.gameObject.name + " Attacks!");
                        GameManager._instance.SubtractLives(1);
                        //HUD.Hit();
                        if (GameManager._instance.lives <= 0)
                        {
                            SceneManager.LoadSceneAsync(4);
                        }
                    }
                }
            }
        }
    }
}