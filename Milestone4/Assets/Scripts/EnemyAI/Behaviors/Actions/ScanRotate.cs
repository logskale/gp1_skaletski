using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlugableAI/Actions/Rotate", fileName = "A_ScanRotate")]

public class ScanRotate : Action
{
    public override void Act (Blackboard blackboard)
    {
        Rotate(blackboard);
    }

    private void Rotate(Blackboard blackboard)
    {
        //stop moving
        blackboard.navMeshAgent.isStopped = true;

        //rotate in place
        blackboard.owningController.transform.Rotate(0f, blackboard.aiStats.searchingTurnSpeed*Time.deltaTime, 0f);
    }
}
