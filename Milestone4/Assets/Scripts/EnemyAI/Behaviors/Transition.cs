using UnityEngine;

/// <summary>
/// The Transition class represents the transition from one State to another State given that the Decision evaluates to true.
/// </summary>

// This allows this class to be serialized by Unity as an asset
[System.Serializable]
public class Transition
{
    [Tooltip("The decision being evaluated by this transition.")]
    public Decision decision;

    [Tooltip("The State to transition to if decision evaluates to true.")]
    public State nextState;
}