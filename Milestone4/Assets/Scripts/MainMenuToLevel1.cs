using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuToLevel1 : MonoBehaviour
{
    private HUDController HUD;

    public void Start()
    {
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();

    }

    public void PlayGame()
    {
        //GameManager._instance.AddLives();
        PlayButtonPressed();
        SceneManager.LoadSceneAsync(1);
    }
    private void PlayButtonPressed()
    {
        if (GameManager._instance.lives == 2)
        {
            HUD.Gain();
            Debug.Log("gained a life");
        }
        else if (GameManager._instance.lives == 1)
        {
            HUD.Gain();
            HUD.Gain();
            Debug.Log("gained a life");
        }
        else if (GameManager._instance.lives == 0)
        {
            HUD.Gain();
            HUD.Gain();
            HUD.Gain();
            Debug.Log("gained a life");
        }
        GameManager._instance.ResetLives();
        GameManager._instance.ResumeGame();
    }
}
