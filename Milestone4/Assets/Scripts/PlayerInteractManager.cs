using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This script sits on an object within our Player's hierarchy that has our InteractTrigger on it.
/// Here we will keep track of any interactables near (in front of) the player, and be able to
/// properly interact with them when the input occurs.
/// </summary>

public class PlayerInteractManager : MonoBehaviour
{
    #region Variables

    /// <summary>
    /// Note: We keep track of GameObjects rather than IInteractable objects because the
    /// GetComponent<T>() call is expensive. We won't necessarily interact with every
    /// object that enters this trigger, so it's more efficient to simply grab a reference
    /// to the IInteractable script on a given object when we try to interact with them.
    /// </summary>
    [Tooltip("A list of all GameObjects that can be interacted with")]
    List<GameObject> interactableObjects = new List<GameObject>();

    [Tooltip("A reference to our PlayerBody that we can pass through to object's Interact() functions")]
    [SerializeField] private PlayerBody playerBody;

    [Tooltip("This event is called when there goes from being 0 to more than 0 interactable objects")]
    public UnityEvent OnInteractablesExist;
    [Tooltip("This event is called when there goes from being more than 0 to 0 interactable objects")]
    public UnityEvent OnInteractablesDoNotExist;

    #endregion

    #region Unity Functions

    private void OnTriggerEnter(Collider other)
    {
        // If an interactable object enters the interact trigger
        if(other.CompareTag("Interactable"))
        {
            TrackObject(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // If the object exiting the trigger is an interactable object
        if(other.CompareTag("Interactable"))
        {
            StopTrackingObject(other.gameObject);
        }
    }

    #endregion

    #region Custom Functions

    /// <summary>
    /// This function is called by the PlayerBody when the interact action occurs.
    /// Interact with an object if there are any.
    /// </summary>
    public void Interact()
    {
        // Do nothing if there's nothing to interact with
        if (interactableObjects.Count == 0) return;

        // Interact with the first object in the list
        interactableObjects[0].GetComponent<IInteractable>().Interact(this, playerBody);
    }

    #region Object Tracking

    /// <summary>
    /// Track an interactable gameObject for later reference.
    /// </summary>
    /// <param name="objectToTrack">The object to start tracking.</param>
    private void TrackObject(GameObject objectToTrack)
    {
        // Track it in our interactableObjects list
        interactableObjects.Add(objectToTrack);

        // Interactables now exist, so invoke the corresponding event
        if (interactableObjects.Count == 1)
        {
            OnInteractablesExist.Invoke();
        }
    }

    /// <summary>
    /// Stop tracking a given gameObject, if it's being tracked.
    /// This function should be public so if an interactable
    /// needs to destroy or disable itself, it can
    /// notify the PlayerInteractManager.
    /// </summary>
    /// <param name="trackedObject">The object to stop tracking.</param>
    public void StopTrackingObject(GameObject trackedObject)
    {
        // And it's an object that was already in our interactable objects list
        if (interactableObjects.Contains(trackedObject))
        {
            interactableObjects.Remove(trackedObject);

            // Interactables no longer exist, so invoke the corresponding event
            if (interactableObjects.Count == 0)
            {
                OnInteractablesDoNotExist.Invoke();
            }
        }
    }

    #endregion

    #endregion
}
