using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.RestService;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //maximum horizontal move speed

    [Header("Player Movement")]
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    //[SerializeField] private float moveSpeed = 9f;
    //[SerializeField] private float accelSpeed = 10f;
    //[SerializeField] private float decelSpeed = 10f;
    [SerializeField] private float jumpForce = 10f;
    //[SerializeField] private float velPower = 0.9f;
    //[SerializeField] private float frictionAmount = 0.8f;
    //[SerializeField] public float fallDeceleration = 2.0f;
    [SerializeField] private bool isMoving = false;
    [SerializeField] private bool isGrounded = false;
    [SerializeField] private bool isWalledRight = false;
    [SerializeField] private bool isWalledLeft = false;
    [SerializeField] private bool canWallJump = false;

    [Header("Layer Masks")]
    [SerializeField] private LayerMask environmentLayerMask;
    [SerializeField] private LayerMask wallLayerMask;

    [Header("Player Input")]
    private Vector2 movementInput;

    [Header("Player Animation")]
    private SpriteRenderer spriteRenderer;
    private Animator animator;

    //private PlayerController playerController;

    private void Awake()
    {
        // Get component references
        //rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        //boxCollider2D = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        
        
    }

    private void Update()
    {
       
        CheckIsGrounded();
        CheckIsWalledRight();
        CheckIsWalledLeft();
        //flips sprite when changing directions
        if (movementInput.x > 0)
            spriteRenderer.flipX = false;
        else if (movementInput.x < 0)
            spriteRenderer.flipX = true;
    }

    public void SetMovementInput(Vector2 moveInput)
    {
        movementInput = moveInput;
    }

    public void Jump()
    {
        //makes sure we can't jump mid-air
        if (isGrounded)
        {
            //upward force applied to player
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            //activate jump animation
            //animator.SetBool("isJumping", true);
        }
        //wall jumps
        if (isWalledLeft && !isGrounded && canWallJump)
        {
            //force up and right
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.right * (jumpForce / 2), ForceMode2D.Impulse);
            //animator.SetBool("isJumping", true);
            canWallJump = false;
        }
        else if (isWalledRight && !isGrounded && canWallJump)
        {
            //force up and left
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.left * (jumpForce / 2), ForceMode2D.Impulse);
            //animator.SetBool("isJumping", true);
            canWallJump = false;
        }
    }

    //private void MoveHorizontal()
    //{
    //    if (Mathf.Abs(movementInput.x) > 0.01f)
    //    {
    //        //find direction we want to move
    //        float targetSpeed = movementInput.x * moveSpeed;
    //        //find diff between current and previous velocity
    //        float speedDif = targetSpeed - rigidbody.velocity.x;
    //        //determine whether to accelerate or decelerate
    //        float accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? accelSpeed : decelSpeed;
    //        float movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, velPower) * Mathf.Sign(speedDif);
    //        //apply the force multiplied by Vector2.Right
    //        rigidbody.AddForce(movement * Vector2.right, ForceMode2D.Force);
    //        animator.SetBool("isMoving", true);
    //    }
    //    else if (isGrounded)
    //    {
    //        float amount = Mathf.Min(Mathf.Abs(rigidbody.velocity.x), Mathf.Abs(frictionAmount));
    //        //check direction velocity is applied to is correct
    //        amount *= Mathf.Sign(rigidbody.velocity.x);
    //        //apply the friction force oposite of velocity
    //        rigidbody.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
    //    }
    //    //sets isMoving to true if the velocity of the rigidbody is above 0
    //    animator.SetBool("isMoving", Mathf.Abs(rigidbody.velocity.x)>0.01f);
    //    //if charecter is falling, set isFalling to true
    //    animator.SetBool("isFalling", (rigidbody.velocity.y < -0.01f));
    //}

    private void CheckIsGrounded()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, 0.2f, environmentLayerMask);
        isGrounded = (boxCastHit.collider != null);
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("isInAir", !isGrounded);
        if (isGrounded)
        {
            canWallJump = true;
        }
    }

    private void CheckIsWalledRight()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.right, 0.2f, wallLayerMask);
        isWalledRight = (boxCastHit.collider != null);
    }

    private void CheckIsWalledLeft()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.left, 0.2f, wallLayerMask);
        isWalledLeft = (boxCastHit.collider != null);
    }

}
