using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        Playing,
        Paused,
        PlayerWon,
        PlayerLost
    }

    public GameState CurrentGameState { get; private set; }

    public UnityEvent OnGamePaused;
    public UnityEvent OnGameResumed;

    public static GameManager _instance = null;

    private void Awake()
    {
        #region Singleton

        // If an instance of the GameManager does not already exist
        if (_instance == null)
        {
            // Make this object the one that _instance points to
            _instance = this;

            // We want this object to persist between scenes, so don't destroy it on load
            DontDestroyOnLoad(gameObject);
        }
        // Otherwise if an instance already exists and it's not this one
        else
        {
            // Destroy this GameManager
            Destroy(gameObject);
        }

        #endregion
    }

    public void TogglePause()
    {
        if (CurrentGameState == GameState.Playing)
        {
            PauseGame();
        }
        else if (CurrentGameState == GameState.Paused)
        {
            ResumeGame();
        }
    }

    public void PauseGame()
    {
        if (CurrentGameState == GameState.Paused) return;

        CurrentGameState = GameState.Paused;

        Time.timeScale = 0.0f;

        OnGamePaused.Invoke();
    }

    public void ResumeGame()
    {
        if (CurrentGameState == GameState.Playing) return;

        CurrentGameState = GameState.Playing;

        Time.timeScale = 1.0f;

        OnGameResumed.Invoke();
    }
}