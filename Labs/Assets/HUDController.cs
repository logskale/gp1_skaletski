using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HUDController : MonoBehaviour
{

    [SerializeField] private UIDocument UIDoc;
    [SerializeField] private float levelTime;
    [SerializeField] private Sprite heartImage;
    private VisualElement heartsContainer;
    private Label timeLabel;

    // Start is called before the first frame update
    void Start()
    {
        VisualElement root = UIDoc.rootVisualElement;

        timeLabel = root.Q<Label>("timeLeftValue");

        heartsContainer = root.Q<VisualElement>("Lives");

        timeLabel.text = "" + levelTime;

        AddLife(heartsContainer);
        AddLife(heartsContainer);
        AddLife(heartsContainer);

    }

    // Update is called once per frame
    void Update()
    {
        // Check if the "E" key is pressed
        if (Input.GetKeyDown(KeyCode.E))
        {
            AddLife(heartsContainer);
        }

        // Check if the "R" key is pressed
        if (Input.GetKeyDown(KeyCode.R))
        {
            RemoveLife(heartsContainer);
        }
        // Update the levelTime
        if (levelTime > 0)
        {
            levelTime -= Time.deltaTime;
            timeLabel.text = Mathf.Ceil(levelTime).ToString(); // Round up and convert to string
        }
        else
        {
            // Handle the case when levelTime reaches 0 (e.g., end the game or trigger some event)
        }


    }

     
    public void AddLife(VisualElement container)
    {
        Image heart = new Image();
        heart.sprite = heartImage;

        heart.style.paddingTop = 5;
        heart.style.paddingLeft = 0;
        heart.style.paddingRight = 0;

        heart.style.width = 32;
        heart.style.height = 32;

        heart.style.flexGrow = 0;
        heart.style.flexShrink = 0;

        container.Add(heart);
    }
    public void RemoveLife(VisualElement container)
    {
        int heartCount = container.childCount;

        if (heartCount > 0)
        {
            // Remove the last heart (last child element)
            VisualElement lastHeart = container.ElementAt(heartCount - 1);
            container.Remove(lastHeart);
        }
    }
}
