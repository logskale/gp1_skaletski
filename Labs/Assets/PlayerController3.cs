using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class newPlayerController : MonoBehaviour
{
    [Header("Player Input")]
    private PlayerInputActions playerInputActions;
    private Vector2 movementInput;

    // Awake is called before Start() when an object is created or when the level is loaded
    private void Awake()
    {
        // Set up our player actions in code
        // This class name is based on what you named your .inputactions asset
        playerInputActions = new PlayerInputActions();
    }

    private void Start()
    {
        GameManager._instance.OnGamePaused.AddListener(OnPauseReceived);
        GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
    }

    private void OnEnable()
    {
        // Here we can subscribe functions to our
        // input actions to make code occur when
        // our input actions occur
        SubscribeInputActions();

        // We need to enable our "Player" action map so Unity will listen for our input
        SwitchActionMap("Player");
    }

    private void OnDisable()
    {
        // Here we can unsubscribe our functions
        // from our input actions so our object
        // doesn't try to call functions after
        // it is destroyed
        UnsubscribeInputActions();

        // Disable our action map
        SwitchActionMap("None");
    }

    private void OnPauseReceived()
    {
        SwitchActionMap("UI");
    }

    private void OnResumedReceived()
    {
        SwitchActionMap("Player");
    }

    private void SubscribeInputActions()
    {
        // Here we can bind our input actions to functions
        playerInputActions.Player.Move.started += MoveAction;
        playerInputActions.Player.Move.performed += MoveAction;
        playerInputActions.Player.Move.canceled += MoveAction;


        playerInputActions.Player.PauseGame.performed += PauseActionPerformed;

        playerInputActions.UI.PauseGame.performed += PauseActionPerformed;
    }

    private void UnsubscribeInputActions()
    {
        // It is important to unbind and actions that we bind
        // when our object is destroyed, or this can cause issues
        playerInputActions.Player.Move.started -= MoveAction;
        playerInputActions.Player.Move.performed -= MoveAction;
        playerInputActions.Player.Move.canceled -= MoveAction;


        playerInputActions.Player.PauseGame.performed -= PauseActionPerformed;

        playerInputActions.UI.PauseGame.performed -= PauseActionPerformed;

    }

    private void MoveAction(InputAction.CallbackContext context)
    {
        // Read in the Vector2 of our player input.
        movementInput = context.ReadValue<Vector2>();

        Debug.Log("The player is trying to move: " + movementInput);
    }


    private void PauseActionPerformed(InputAction.CallbackContext context)
    {
        GameManager._instance.TogglePause();
    }

    private void SwitchActionMap(string mapName)
    {
        switch (mapName)
        {
            case "Player":
                playerInputActions.UI.Disable();
                playerInputActions.Player.Enable();

                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                break;

            case "UI":
                playerInputActions.Player.Disable();
                playerInputActions.UI.Enable();

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                break;

            case "None":
                playerInputActions.UI.Disable();
                playerInputActions.Player.Disable();
                break;

            defult:
                Debug.LogError("Unrecognized action map specified");
                break;
        }
    }
}