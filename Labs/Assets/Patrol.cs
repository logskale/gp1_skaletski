using System.Collections;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    [SerializeField] private float patrolPause = 1f; // Time to pause at each waypoint
    [SerializeField] private float patrolSpeed = 1f; // Speed at which to patrol between the waypoints

    private Vector2[] waypoints; // Array of random waypoints
    private int currentWaypointIndex = 0; // Index of the current waypoint
    private bool isFlipped = false; // Whether the sprite is flipped

    [SerializeField] private int numPoints = 5;

    // Start the patrolling coroutine
    void Start()
    {
        // Generate a random array of waypoints
        waypoints = new Vector2[numPoints];
        for (int i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = new Vector2(Random.Range(-6, 6), Random.Range(-6, 6));
        }

        StartCoroutine(Patroler());
    }

    // Coroutine that patrols between the waypoints smoothly
    IEnumerator Patroler()
    {
        while (true)
        {
            //// Calculate the distance to the next waypoint
            Vector2 targetPosition = waypoints[currentWaypointIndex];
            float distanceToTarget = Vector2.Distance(transform.position, targetPosition);

            //// Calculate the amount to move towards the target position this frame, using smoothstep
            float movementAmount = Mathf.SmoothStep(0f, 1f, distanceToTarget / patrolSpeed);

            //// Move towards the target position
            //transform.position = Vector2.MoveTowards(transform.position, targetPosition, .5f);


            //new movement
            Vector2 originToDest = targetPosition-(Vector2)transform.position;
            Vector2 distance = originToDest.normalized;
            transform.position = (Vector2)transform.position + (distance*patrolSpeed*Time.deltaTime);


            // Yield to wait for the sprite to move closer to the target position
            yield return new WaitForSeconds(movementAmount / patrolSpeed);

            // Flip the sprite if moving in the opposite direction
            if (currentWaypointIndex % 2 == 0)
            {
                isFlipped = !isFlipped;
                transform.localScale = isFlipped ? new Vector2(-5, 5) : new Vector2(5, 5);
            }

            // Increment the waypoint index
            currentWaypointIndex++;

            // If we've reached the end of the waypoint array, wrap around to the beginning
            if (currentWaypointIndex == waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }
    }
}