using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlayerControl : MonoBehaviour
{
    [SerializeField]private float moveSpeed = 5.0f; // Expose the movement speed to the Unity editor

    private void Update()
    {
        // Get input from the arrow keys or WASD keys
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calculate the movement direction
        Vector3 movement = new Vector3(horizontalInput, verticalInput, 0.0f);

        // Normalize the movement vector to prevent faster diagonal movement
        movement.Normalize();

        // Move the object based on the input
        transform.Translate(movement * moveSpeed * Time.deltaTime);
    }
}