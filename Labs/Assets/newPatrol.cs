using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newPatrol : MonoBehaviour
{
	[SerializeField] private float speed = .1f;
	//public List<Vector3> waypoints;
	private Vector2[] waypoints; // Array of random waypoints
	[SerializeField] private int numPoints = 5;
	private bool isFlipped = false; // Whether the sprite is flipped
	private SpriteRenderer spriteRenderer;
	private Rigidbody2D rb;
	private float oldX=0f;
	// start patrolling immediately, but we could also make a method to trigger this later
	// save the coroutine, so we can stop it if we need to
	void Start()
	{
		 spriteRenderer = GetComponent<SpriteRenderer>();
		rb = GetComponent<Rigidbody2D>();
		waypoints = new Vector2[numPoints];
		for (int i = 0; i < waypoints.Length; i++)
		{
			waypoints[i] = new Vector2(Random.Range(-6, 6), Random.Range(-6, 6));
		}
		path = StartCoroutine(PatrolWaypoints());
	}

	public virtual void StopPatrolling()
	{
		StopCoroutine(path);
	}

    public void Update()
    {
		
		if ((rb.position.x - oldX) > 0f) // We check with > and < to account for analog input methods like joysticks that aren't always exactly at a value of 1
			spriteRenderer.flipX = true;
		// Otherwise if we are inputting left, face left
		else if ((rb.position.x - oldX) < 0f)
			spriteRenderer.flipX = false;

		oldX = rb.position.x;
	}

    public IEnumerator PatrolWaypoints()
	{
		// Flip the sprite if moving in the opposite direction

	
		// path forever, unless StopPatrolling is called
		while (true)
		{
			
			// iterate through all points
			foreach (Vector3 point in waypoints)
			{
				// now with each point, I want to interpolate to it, so I'll use MoveTowards
				// which will at most move me at the given speed
				while (transform.position != point)
				{
					transform.position = Vector3.MoveTowards(transform.position, point, speed);
					yield return null;
				}
			}

			yield return null;
		}
	}

	private Coroutine path;
	
}