using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class PauseUI : MonoBehaviour
{
    private VisualElement root;
    private Button resumeButton;
    private Button mainMenuButton;
    //private Button quitGameButton;

    public void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        resumeButton = root.Q<Button>("resume-button");
        mainMenuButton = root.Q<Button>("mainmenu-button");
        //quitGameButton = root.Q<Button>("quit-button");

        resumeButton.clicked += ResumeButtonPressed;
        mainMenuButton.clicked += MainMenuButtonPressed;
        //quitGameButton.clicked += QuitButtonPressed;

        GameManager._instance.OnGamePaused.AddListener(OnPauseReceived);
        GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
        OnResumedReceived();
    }

    private void OnDestroy()
    {
        resumeButton.clicked -= ResumeButtonPressed;
        mainMenuButton.clicked -= MainMenuButtonPressed;
        //quitGameButton.clicked -= QuitButtonPressed;
    }

    private void ResumeButtonPressed()
    {
        Debug.Log("Game resumed!");
        GameManager._instance.ResumeGame();
    }
    private void MainMenuButtonPressed()
    {
        Debug.Log("Go to main menu!");
        //SceneManager.LoadScene("MainMenu");
    }

    private void QuitButtonPressed()
    {
        Application.Quit(0);
    }

    private void OnPauseReceived()
    {
        root.style.visibility = Visibility.Visible;
    }

    private void OnResumedReceived()
    {
        root.style.visibility = Visibility.Hidden;
    }
}
