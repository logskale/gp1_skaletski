using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotating : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 90.0f; // Expose the speed of rotation to the Unity editor

    private void Update()
    {
        // Rotate the sprite around its local up axis (Y-axis) at the specified speed
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
