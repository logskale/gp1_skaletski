using UnityEngine;

public class KinematicObjectController : MonoBehaviour
{
    [SerializeField]private Transform pointA;
    [SerializeField] private Transform pointB;
    [SerializeField] private float moveSpeed = 2.0f;

    private Rigidbody2D rb;
    private Transform targetPoint;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        targetPoint = pointA;
    }

    private void FixedUpdate()
    {
        // Move towards the target point using MovePosition.
        Vector2 newPosition = Vector2.MoveTowards(rb.position, targetPoint.position, moveSpeed * Time.fixedDeltaTime);
        rb.MovePosition(newPosition);

        // Check if we've reached the target point and switch to the other point.
        if (Vector2.Distance(rb.position, targetPoint.position) < 0.01f)
        {
            targetPoint = (targetPoint == pointA) ? pointB : pointA;
        }
    }
}
