using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 10f;


    private Vector2 movement;
    private bool isFacingRight = true;


    private void FixedUpdate()
    {
        // Get player input for movement.
        float horizontalInput = Input.GetAxis("Horizontal");

        // Apply force for horizontal movement.
        movement = new Vector2(horizontalInput * moveSpeed, rb.velocity.y);
        rb.velocity = movement;

        // Flip the character if changing direction.
        if ((horizontalInput > 0 && !isFacingRight) || (horizontalInput < 0 && isFacingRight))
        {
            FlipCharacter();
        }

        // Check for jump input and apply an upward force if grounded and allowed to jump.
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space) )
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
           
        }
    }

    // Flip the character sprite along the Y-axis.
    private void FlipCharacter()
    {
        isFacingRight = !isFacingRight;
        Vector3 newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
    }



    
}
