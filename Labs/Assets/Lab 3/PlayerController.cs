using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; //use InputSystem package

public class PlayerController : MonoBehaviour
{
    [SerializeField] PlayerMovement playerMovement;

    private PlayerInputActions playerInputActions;



    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
    }
    private void OnEnable()
    {
        playerInputActions.Player.Move.started +=MoveAction;
        playerInputActions.Player.Move.performed += MoveAction;
        playerInputActions.Player.Move.canceled += MoveAction;

        playerInputActions.Player.Push.canceled += PushActionPerformed;

        //enable our player action map so unity will listen for our input
        playerInputActions.Player.Enable();
    }
    private void OnDisable()
    {
        playerInputActions.Player.Move.started -= MoveAction;
        playerInputActions.Player.Move.performed -= MoveAction;
        playerInputActions.Player.Move.canceled -= MoveAction;

        playerInputActions.Player.Push.performed -= PushActionPerformed;

        playerInputActions.Player.Disable();
    }
    private void MoveAction (InputAction.CallbackContext context)
    {
        Vector2 movementInput = context.ReadValue<Vector2>();
        Debug.Log("The player is trying to move" + movementInput);
        playerMovement.SetMovementInput(movementInput);
        
    }
    private void PushActionPerformed(InputAction.CallbackContext context)
    {
        Debug.Log("The player is tryign to push");
    }
}
