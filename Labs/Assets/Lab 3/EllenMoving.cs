using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.RestService;
using UnityEngine;

// We can require components that our script is dependent on
// If we add our PlayerCharacter script to an object that's
// missing one of these components, it will automatically add it
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerMovement : MonoBehaviour
{
    #region Variables

    //We need to get a reference to the RigidBody2D component of the player object in order to control its physics
    [Header("Player Movement")]
    private Rigidbody2D rigidbody;
    private BoxCollider2D boxCollider2D;

    [Tooltip("The max horizontal speed of this character")]
    [SerializeField] private float moveSpeed = 9f;
    [Tooltip("The rate at which this character accelerates")]
    [SerializeField] private float accelerationRate = 9f;
    [Tooltip("The rate at which this character decelerates")]
    [SerializeField] private float decelerationRate = 9f;
    [Tooltip("A modifier for this character's acceleration to scale acceleration with velocity")]
    [SerializeField] private float velPower = 0.9f;
    [Tooltip("A value used to stop the character upon loss of input more quickly")]
    [SerializeField] private float frictionAmount = 0.8f;
    [Tooltip("The amount of power used by this character to jump")]
    [SerializeField] private float jumpForce = 10f;
    private bool isGrounded = false;
    private bool isWalled = false;
    [SerializeField] private LayerMask environmentLayerMask;
    [SerializeField] private LayerMask wallLayerMask;

    [Header("Player Input")]
    private Vector2 movementInput;

    [Header("Player Animation")]
    private SpriteRenderer spriteRenderer;
    private Animator animator;

    #endregion

    #region Unity Functions

    // Awake is called before Start() when an object is created or when the level is loaded
    private void Awake()
    {
        // Get component references
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (Input.GetButton("Move") == true && !isWalled)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
        //if (Input.GetButton("Push") == true || CheckIsPushing() == true)
        //{
        //    animator.SetBool("isPushing", true);
        //}
        //else
        //{
        //    animator.SetBool("isPushing", false);
        //}

        // Check for the jump input keys (spacebar or 'W')
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) )
        {
            Jump(); // Call the Jump function when the key is pressed down
            animator.SetBool("isJumping", true);
        }
        else
        {
            animator.SetBool("isJumping", false);
        }

        // The first thing we should do every frame is
        // check if we are grounded. Then if we need this
        // information for any other operations, we'll
        // know our correct ground status, since we
        // checked it first.
        CheckIsGrounded();
        CheckIsWalled();
        

        // Remember, any time we want to do things
        // with physics over time (like moving), we
        // need to do them in FixedUpdate() rather
        // than Update().
        MoveHorizontal();

        

    }

    // Update is called once per frame
    void Update()
    {
        // Based on our movement input, we can flip our
        // sprite to face either left or right depending
        // on the x value of the input Vector2D.
        // If we are inputting right, face right
        if (movementInput.x > 0) // We check with > and < to account for analog input methods like joysticks that aren't always exactly at a value of 1
            spriteRenderer.flipX = false;
        // Otherwise if we are inputting left, face left
        else if (movementInput.x < 0)
            spriteRenderer.flipX = true;

        // Similarly, we can tell from our movement input
        // if we should be moving. We can store this value
        // for use in our animator.
        if (!isWalled)
        animator.SetBool("isRunning", (movementInput.x != 0f));
    }

    #endregion

    #region Custom Functions

    /// <summary>
    /// Called from the PlayerController, this sets the
    /// movementInput whenever the Move input action occurs.
    /// </summary>
    /// <param name="moveInput">The Vector2 value produced by the input.</param>
    public void SetMovementInput(Vector2 moveInput)
    {
        movementInput = moveInput;
    }

    #region Player Movement

    /// <summary>
    /// Makes the character jump upwards using physics.
    /// </summary>
    private void Jump()
    {
        // We need to first make sure that we are on the
        // ground before we make our character jump. We
        // don't want to be able to jump in mid-air.
        if (isGrounded)
        {
            // We can apply an upwards force on our
            // character using the Impulse force mode
            // for a one-time burst of velocity in a
            // given direction: In this case, up.
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

            // We need to make sure to tell our animator
            // that we are beginning our jump so we can
            // activate our jump animation properly.
            animator.SetBool("isJumping",true);
        }
        if (isWalled && !isGrounded)
        {
            // We can apply an upwards force on our
            // character using the Impulse force mode
            // for a one-time burst of velocity in a
            // given direction: In this case, up.
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);


        }
    }

    /// <summary>
    /// This method will be called when the jump button is released.
    /// It will cut the character's vertical velocity in half (if
    /// going upwards), to give the effect of an adjustable height
    /// jump as is present in many platformers.
    /// </summary>
    private void CancelJump()
    {
        // If our player is going upwards when the
        // jump button is released (when this
        // function is called)
        if (rigidbody.velocity.y > 0)
        {
            // We can reduce our current y
            // velocity to cut our jump short
            rigidbody.AddForce(Vector2.down * rigidbody.velocity.y * 0.5f, ForceMode2D.Impulse);
        }
    }

    /// <summary>
    /// A movement function called from FixedUpdate that
    /// moves the player with rigidbody forces based on
    /// the movementInput polled in Update.
    /// </summary>
    private void MoveHorizontal()
    {
        // Let's move our character based on our
        // movementInput value that we polled in Update().
        // We'll move by applying forces on our character.
        // This means we'll have acceleration and deceleration.

        // Here we can check if we are trying to move. If we
        // are, then handle movement as normal.
        if (Mathf.Abs(movementInput.x) > 0.01f)
        {
            // Here we calculate the direction we want to move in
            // and our desired velocity in that direction.
            float targetSpeed = movementInput.x * moveSpeed;

            // Here we calculate the difference between our current
            // velocity and our previously calculated desired velocity.
            float speedDif = targetSpeed - rigidbody.velocity.x;

            // Here we determine if we are accelerating or decelerating.
            float accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? accelerationRate : decelerationRate;

            // Here we apply the acceleration to the speed difference
            // then raise the result to a set power so acceleration
            // increases with higher speeds and vice versa.
            float movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, velPower) * Mathf.Sign(speedDif);

            // Finally, we apply the force multiplied by Vector2.right
            // to ensure that the force only affects the horizontal movement.
            rigidbody.AddForce(movement * Vector2.right, ForceMode2D.Force);
        }
        // Otherwise, if we are not moving and grounded, we can apply
        // some acceleration in the opposite direction of our
        // horizontal momentum to bring us to a snappier stop.
        // In this way, we are simulating extra friction to stop smoothly.
        else if (isGrounded)
        {
            // We use either the friction amount or our horizontal
            // velocity amount to counteract the velocity.
            float amount = Mathf.Min(Mathf.Abs(rigidbody.velocity.x), Mathf.Abs(frictionAmount));

            // Ensure that the direction we're applying friction in
            // is in the correct direction depending on our velocity.
            amount *= Mathf.Sign(rigidbody.velocity.x);

            //Apply the friction force in the direction opposite our velocity.
            rigidbody.AddForce(Vector2.right * -amount, ForceMode2D.Impulse);
        }

        // Here we can get the velocity of our rigidbody
        // and use that to determine if we are moving or
        // not. Then we can use that information to tell
        // our animation if we should be running or not.
        // We only want velocity in the x dimension,
        // because that's what tells us if we're running.
        // We take the absolute value of the x velocity,
        // because we don't care about direction, just
        // the magnitude of the speed.
        animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));

        // We can also tell if we're going up or down using
        // rigidbody velocity. Because we have an animation
        // for going up and down separately in the air, this
        // is important to know.
        animator.SetBool("isFalling", (rigidbody.velocity.y < 0f));
    }

    /// <summary>
    /// This function will be called every physics
    /// frame in FixedUpdate and will determine if
    /// the character is on the ground or not.
    /// </summary>
    private void CheckIsGrounded()
    {
        // Here we will use a boxCast to check a rectangular
        // area under our player and check if there are any
        // objects in the Environment layer there.
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, 0.1f, environmentLayerMask);

        // Based on whether or not we found a collider with
        // our boxCast, we will know if we are on the ground.
        isGrounded = (boxCastHit.collider != null);

        // Also, don't forget to let the animator know whether
        // or not we're grounded so we can play the right animation.
        animator.SetBool("isGrounded", isGrounded);
    }
    private bool CheckIsPushing()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.right, 0.1f, environmentLayerMask);
        //check if we hit somethign with boxcast
        bool isPushing = (boxCastHit.collider != null);
        return isPushing;
    }
    private void CheckIsWalled()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.right, 0.2f, wallLayerMask);
        isWalled = (boxCastHit.collider != null);
        //if (isGrounded == false)
        //{
        //    animator.SetBool("jumpAgain", isWalled);
        //}


    }


    #endregion

    #endregion
}
