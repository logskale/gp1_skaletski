using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    private VisualElement root;
    private Button resumeButton;
    private Button quitButton;
    private Button creditsGameButton;
    private VisualElement credits;
    private HUDController HUD;
    private bool isShown;

    public void Start()
    {
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();

        root = GetComponent<UIDocument>().rootVisualElement;
        resumeButton = root.Q<Button>("resume-button");
        quitButton = root.Q<Button>("quit-button");
        creditsGameButton = root.Q<Button>("credits-button");
        credits = root.Q<VisualElement>("Credits");
        isShown = false;

        resumeButton.clicked += ResumeButtonPressed;
        quitButton.clicked += QuitButtonPressed;
        creditsGameButton.clicked += CreditsButtonPressed;

        //GameManager._instance.OnGamePaused.AddListener(OnPauseReceived);
        //GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
        //OnResumedReceived();
        root.style.visibility = Visibility.Visible;
        credits.style.visibility = Visibility.Hidden;
    }

    private void OnDestroy()
    {
        resumeButton.clicked -= ResumeButtonPressed;
        quitButton.clicked -= QuitButtonPressed;
        creditsGameButton.clicked -= CreditsButtonPressed;
    }

    private void ResumeButtonPressed()
    {
        if (GameManager._instance.lives == 2)
        {
            HUD.Gain();
            Debug.Log("gained a life");
        }
        else if (GameManager._instance.lives == 1)
        {
            HUD.Gain();
            HUD.Gain();
            Debug.Log("gained a life");
        }
        else if (GameManager._instance.lives == 0)
        {
            HUD.Gain();
            HUD.Gain();
            HUD.Gain();
            Debug.Log("gained a life");
        }
        GameManager._instance.ResetLives();
        SceneManager.LoadScene("Level1");
        GameManager._instance.ResumeGame();
    }
    private void QuitButtonPressed()
    {
        Application.Quit();


    }

    private void CreditsButtonPressed()
    {
        if (isShown)
        {
            credits.style.visibility = Visibility.Hidden;
            isShown = !isShown;
        }
        else
        {
            credits.style.visibility = Visibility.Visible;
            isShown = !isShown;
        }

    }

    private void OnPauseReceived()
    {
        root.style.visibility = Visibility.Visible;
        credits.style.visibility = Visibility.Hidden;
    }

    private void OnResumedReceived()
    {
        root.style.visibility = Visibility.Hidden;
        credits.style.visibility = Visibility.Hidden;
    }
}