using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [Header("Player Movement")]
    [SerializeField] PlayerMovement playerMovement;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private float moveSpeed = 8f;
    [SerializeField] private float jumpForce = 16f;
    [SerializeField] private float jumpPushForce = 16f;
    [SerializeField] private float fallForce = 10f;
    [SerializeField] private float accelSpeed = 10f;
    [SerializeField] private float decelSpeed = 10f;
    [SerializeField] private float velPower = 0.9f;
    [SerializeField] private float frictionAmount = 0.8f;
    [SerializeField] private float fallDeceleration = 2.0f;
    [SerializeField] private float boxcastLength = .2f;
    [SerializeField] private bool isFacingRight = true;
    [SerializeField] private bool isGrounded = false;
    [SerializeField] private bool isMoving = false;
    [SerializeField] private bool isWalledRight = false;
    [SerializeField] private bool isWalledLeft = false;
    [SerializeField] private bool canWallJump = false;
    [SerializeField] private bool wallJumpLeft= false;
    [SerializeField] private bool wallJumpRight = false;

    [Header("Dash")]
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private float dashingPower = 24f;
    [SerializeField] private float dashingTime = 0.5f;
    [SerializeField] private float dashingCooldown = 0.8f;
    private Vector2 dashingDir;
    private bool isDashing;
    private bool canDash=true;

    private bool wasGrounded = false;

    [SerializeField] private AudioClip walking;
    [SerializeField] private AudioSource source;
    [SerializeField] private AudioSource landingSoundClip;
    private PlayerMovement pm;

    [Header("Layer Masks")]
    [SerializeField] private LayerMask environmentLayerMask;
    [SerializeField] private LayerMask wallLayerMask;

    [Header("Player Animation")]
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;

    [Header("Player Input")]
    private Vector2 movementInput;

    private PlayerInputActions playerInputActions;
    private float horizontal;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
    }

    private void OnEnable()
    {
        playerInputActions.Player.Move.started += MoveAction;
        playerInputActions.Player.Move.performed += MoveAction;
        playerInputActions.Player.Move.canceled += MoveAction;

        playerInputActions.Player.Dash.performed += DashActionPerformed;
        playerInputActions.Player.Jump.performed += JumpActionPerformed;

        playerInputActions.Player.PauseGame.performed += PauseActionPerformed;

        //enable our player action map so unity will listen for our input
        playerInputActions.Player.Enable();
    }

    
    private void OnDisable()
    {
        playerInputActions.Player.Move.started -= MoveAction;
        playerInputActions.Player.Move.performed -= MoveAction;
        playerInputActions.Player.Move.canceled -= MoveAction;

        playerInputActions.Player.Dash.performed -= DashActionPerformed;
        playerInputActions.Player.Jump.performed -= JumpActionPerformed;

        playerInputActions.Player.PauseGame.performed -= PauseActionPerformed;

        playerInputActions.Player.Disable();
    }

    private void Update()
    {
        if (isDashing)
        {
            return;
        }


        //updates the checks
        IsGrounded();
        CheckIsWalledLeft();
        CheckIsWalledRight();
        //flips chaercter based on direction moved
        if (!isFacingRight && horizontal < 0f)
        {
            Flip();
        }
        else if (isFacingRight && horizontal > 0f)
        {
            Flip();
        }
        //sets moving animation
        if (Mathf.Abs(rigidbody.velocity.x) > 0.01f)
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }
        //if charecter is falling, set isFalling to true
        animator.SetBool("isFalling", (rigidbody.velocity.y < -0.01f));
        if (Mathf.Abs(rigidbody.velocity.x) > 0.01f && IsGrounded())
        {
            animator.SetBool("isMoving", true);
            // Check if the walking sound is not already playing
            if (!source.isPlaying || source.clip != walking)
            {
                // Play the walking sound
                source.clip = walking;
                source.Play();
            }
        }
        else
        {
            animator.SetBool("isMoving", false);
            // Stop the walking sound
            source.Stop();
        }
    }

    private void FixedUpdate()
    {
        if (isDashing)
        {
            return;
        }
        rigidbody.velocity = new Vector2(horizontal * moveSpeed, rigidbody.velocity.y);

        //speed limiting to the right
        float overSpeedX = Mathf.Abs(rigidbody.velocity.x) - moveSpeed;

        //speed limiting up direction
        float overSpeedY = Mathf.Abs(rigidbody.velocity.y) - moveSpeed;

        //if we are going too fast to the right or left, correct it
        if (overSpeedX > 0f)
        {
            //counteract any speed over the limit
            rigidbody.AddForce(Vector2.left * overSpeedX * (Mathf.Sign(rigidbody.velocity.x)), ForceMode2D.Impulse);
        }

        //if we are going too fast up or down, correct it
        if (overSpeedY > 0f)
        {
            rigidbody.AddForce(Vector2.down * overSpeedY * (Mathf.Sign(rigidbody.velocity.y)), ForceMode2D.Impulse);
        }
        //if you can wall jump left, wall jump
        if (wallJumpLeft && !isGrounded)
        {
            rigidbody.AddForce(Vector2.up * jumpForce * 3, ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.left * jumpPushForce, ForceMode2D.Impulse);
            wallJumpLeft = false;
        }else if (wallJumpRight && !isGrounded)
        {
            rigidbody.AddForce(Vector2.up * jumpForce*3 , ForceMode2D.Impulse);
            rigidbody.AddForce(Vector2.right * jumpPushForce, ForceMode2D.Impulse);
            wallJumpRight = false;
            
        }
        
    }

    //move the player
    public void MoveAction(InputAction.CallbackContext context)
    {
        Vector2 movementInput = context.ReadValue<Vector2>();
        //playerMovement.SetMovementInput(movementInput);
        horizontal = context.ReadValue<Vector2>().x;
        
        

    }

    //for future implementations
    private void DashActionPerformed(InputAction.CallbackContext context)
    {
        Debug.Log("The player is tryign to dash");
        StartCoroutine(Dash());
        
    }

    private IEnumerator Dash()
    {
        
        canDash = false;
        isDashing = true;
        float originalGravity = rigidbody.gravityScale;
        rigidbody.gravityScale = 0;
        if (!isFacingRight)
        {
            rigidbody.velocity = new Vector2(dashingPower, 0f);
        }
        else
        {
            rigidbody.velocity = new Vector2(-dashingPower, 0f);

        }
        trailRenderer.emitting = true;
        yield return new WaitForSeconds(dashingTime);
        trailRenderer.emitting = false;
        rigidbody.gravityScale = originalGravity;
        isDashing = false;
        yield return new WaitForSeconds(dashingCooldown);
        canDash = true;
    }

    public void JumpActionPerformed(InputAction.CallbackContext context)
    {
        //sees if the player can jump
        animator.SetBool("isJumping", false);
        if (context.performed && IsGrounded())
        {
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            animator.SetBool("isJumping", true);
        }
        //sets up wall jumping
        if (context.performed && canWallJump && isWalledLeft)
        {
            wallJumpRight = true;
            animator.SetBool("isJumping", true);
            canWallJump = false;
        }
        else if (context.performed && canWallJump && isWalledRight)
        {
            wallJumpLeft = true;
            animator.SetBool("isJumping", true);
            canWallJump = false;
        }

        //if you let go of the space bar, force charecter downwards
        if (context.canceled && rigidbody.velocity.y > 0.01f)
        {
            rigidbody.AddForce(Vector2.down * fallForce, ForceMode2D.Impulse);
            animator.SetBool("isJumping", false);
        }
        

    }

    //checks to see if the player is on the ground
    private bool IsGrounded()
    {
        //box casts down to see if player is on the ground
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, boxcastLength, environmentLayerMask);
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("jumpingAgain", isGrounded);
        animator.SetBool("isInAir", !isGrounded);
        if (!wasGrounded && boxCastHit.collider != null)
        {
            PlayLandingSound(); // Play the landing sound
        }
        //wall jump reset
        if (isGrounded)
        {
            canWallJump = true;
        }
        wasGrounded = isGrounded;
        //returns if player is grounded
        return isGrounded = (boxCastHit.collider != null);
        
    }
    //checks to see if there is a wall to the right of the player
    private void CheckIsWalledRight()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.right, boxcastLength, wallLayerMask);
        isWalledRight = (boxCastHit.collider != null);
    }
    //checks to see if there is a wall to the left of the player
    private void CheckIsWalledLeft()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.left, boxcastLength, wallLayerMask);
        isWalledLeft = (boxCastHit.collider != null);
    }
    //slips player when moving in new direction
    private void Flip()
    {
        isFacingRight = !isFacingRight;
        spriteRenderer.flipX = !spriteRenderer.flipX;


    }

    private void PauseActionPerformed(InputAction.CallbackContext context)
    {
        GameManager._instance.TogglePause();
    }

    private void PlayLandingSound()
    {
        landingSoundClip.Play();
        
    }

}
