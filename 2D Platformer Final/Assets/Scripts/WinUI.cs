using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class WinUI : MonoBehaviour
{
    private VisualElement root;
    private Button mainMenuButton;
    private HUDController HUD;
    // Start is called before the first frame update
    void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;
        mainMenuButton = root.Q<Button>("mainmenu-button");

        mainMenuButton.clicked += MainMenuButtonPressed;
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();
        GameManager._instance.OnGameWin.AddListener(OnWinReceived);
        GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
        OnResumedReceived();
        root.style.visibility = Visibility.Hidden;
    }

    private void OnDestroy()
    {
        mainMenuButton.clicked -= MainMenuButtonPressed;
    }



    private void MainMenuButtonPressed()
    {
        if (GameManager._instance.lives == 3)
        {
            HUD.Hit();
            HUD.Hit();
            HUD.Hit();
            GameManager._instance.SubtractLives(3);
        }
        else if (GameManager._instance.lives == 2)
        {
            HUD.Hit();
            HUD.Hit();
            GameManager._instance.SubtractLives(2);
        }
        else if (GameManager._instance.lives == 1)
        {
            HUD.Hit();
            GameManager._instance.SubtractLives(1);
        }
        Debug.Log("Go to main menu!");
        GameManager._instance.ResumeGame();
        root.style.visibility = Visibility.Hidden;
        SceneManager.LoadScene("MainMenu");
        
    }

    private void OnWinReceived()
    {
        root.style.visibility = Visibility.Visible;
    }

    private void OnResumedReceived()
    {
        root.style.visibility = Visibility.Hidden;
    }
}
