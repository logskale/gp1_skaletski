using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class PauseUI : MonoBehaviour
{
    private VisualElement root;
    private Button resumeButton;
    private Button mainMenuButton;
    private Button creditsGameButton;
    private VisualElement credits;
    private HUDController HUD;
    private bool isShown;

    public void Start()
    {
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();

        root = GetComponent<UIDocument>().rootVisualElement;
        resumeButton = root.Q<Button>("resume-button");
        mainMenuButton = root.Q<Button>("mainmenu-button");
        creditsGameButton = root.Q<Button>("credits-button");
        credits = root.Q<VisualElement>("Credits");
        isShown = false;

        resumeButton.clicked += ResumeButtonPressed;
        mainMenuButton.clicked += MainMenuButtonPressed;
        creditsGameButton.clicked += CreditsButtonPressed;

        GameManager._instance.OnGamePaused.AddListener(OnPauseReceived);
        GameManager._instance.OnGameResumed.AddListener(OnResumedReceived);
        OnResumedReceived();
    }

    private void OnDestroy()
    {
        resumeButton.clicked -= ResumeButtonPressed;
        mainMenuButton.clicked -= MainMenuButtonPressed;
        creditsGameButton.clicked -= CreditsButtonPressed;
    }

    private void ResumeButtonPressed()
    {
        Debug.Log("Game resumed!");
        GameManager._instance.ResumeGame();
    }
    private void MainMenuButtonPressed()
    {
        if (GameManager._instance.lives == 3)
        {
            HUD.Hit();
            HUD.Hit();
            HUD.Hit();
            GameManager._instance.SubtractLives(3);
        }
        else if (GameManager._instance.lives == 2)
        {
            HUD.Hit();
            HUD.Hit();
            GameManager._instance.SubtractLives(2);
        }
        else if (GameManager._instance.lives == 1)
        {
            HUD.Hit();
            GameManager._instance.SubtractLives(1);
        }

        Debug.Log("Go to main menu!");
        SceneManager.LoadScene("MainMenu");
        GameManager._instance.ResumeGame();


    }

    private void CreditsButtonPressed()
    {
        if (isShown)
        {
            credits.style.visibility = Visibility.Hidden;
            isShown = !isShown;
        }
        else
        {
            credits.style.visibility = Visibility.Visible;
            isShown = !isShown;
        }
            
    }

    private void OnPauseReceived()
    {
        root.style.visibility = Visibility.Visible;
        credits.style.visibility = Visibility.Hidden;
    }

    private void OnResumedReceived()
    {
        root.style.visibility = Visibility.Hidden;
        credits.style.visibility = Visibility.Hidden;
    }
}
