using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyHit : MonoBehaviour
{
    private HUDController HUD;
    private AudioSource source;
    [SerializeField] private AudioSource hitSoundClip;
    [SerializeField] private ParticleSystem hitParticles; // Reference to the Particle System
    [SerializeField] AnimationCurve curve;
    [SerializeField] private float shakeTime = 1f;
    private ScreenShake screenShake;

    private void Start()
    {
        HUD = GameObject.Find("HUD").GetComponent<HUDController>();
        source = GetComponent<AudioSource>(); // Get the AudioSource component from the same GameObject as this script.
        screenShake = Camera.main.GetComponent < ScreenShake>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            //ScreenShake.myInstance.StartCoroutine(ScreenShake.myInstance.Shake());

            screenShake.Shake();

            Debug.Log("You have been hurt");
            GameManager._instance.SubtractLives(1);
            HUD.Hit();
            GameManager._instance.DeadGame();

            // Play the hit sound
            if (source != null && hitSoundClip != null)
            {
                hitSoundClip.Play();
            }

                // Play the hit particles
                if (hitParticles != null)
                {
                    hitParticles.Play();
                }
            }
    }

    //public IEnumerator Shake()
    //{

    //    Vector3 startPosition = transform.position;
    //    float TimeUsed = 0f;
    //    while (TimeUsed < shakeTime)
    //    {
    //        TimeUsed += Time.deltaTime;
    //        float strength = curve.Evaluate(TimeUsed / shakeTime);
    //        transform.position = startPosition + Random.insideUnitSphere * strength;
    //        yield return null;
    //    }
    //    transform.position = startPosition;
    //}


}
