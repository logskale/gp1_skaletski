using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed;
    private Vector3 target;
    private Vector3 velocity;
    private Vector3 previousPosition;
    private bool flipped;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform[] waypoints;
    void Start()
    {
        target = waypoints[1].position;
    }
    public virtual IEnumerator setTarget(Vector3 position)
    {
        yield return new WaitForSeconds(1f);
        target = position;
        FaceTowards(position - transform.position);

    }

    public virtual void FaceTowards(Vector3 direction)
    {
        if (direction.x < 0f)
        {
            spriteRenderer.flipX=true;
        }
        else
        {
            spriteRenderer.flipX = false;
        }
    }

    public void Update()
    {
        Movement();
    }

    public void Movement()
    {
        velocity = ((transform.position - previousPosition) / Time.deltaTime);
        previousPosition = transform.position;

        if(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }
        else
        {
            if (target == waypoints[0].position)
            {
                if (flipped)
                {
                    flipped = !flipped;
                    StartCoroutine("setTarget", waypoints[1].position);
                }

            }
            else
            {
                if (!flipped)
                {
                    flipped=!flipped;
                    StartCoroutine("setTarget", waypoints[0].position);
                }
            }
            
        }
    }

}
