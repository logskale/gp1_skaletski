using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        Playing,
        Paused,
        PlayerWon,
        PlayerLost
    }

    public GameState CurrentGameState { get; private set; }

    public UnityEvent OnGamePaused;
    public UnityEvent OnGameResumed;
    public UnityEvent OnGameDead;
    public UnityEvent OnGameWin;
    HUDController HUD;
    public float lives;
    public bool won;
    public AudioSource source;
    [SerializeField] private AudioClip clipDead;
    [SerializeField] private AudioClip clipWin;

    public static GameManager _instance = null;

    private void Awake()
    {
        won = false;
        #region Singleton
        lives = 3;
        // If an instance of the GameManager does not already exist
        if (_instance == null)
        {
            // Make this object the one that _instance points to
            _instance = this;

            // We want this object to persist between scenes, so don't destroy it on load
            DontDestroyOnLoad(gameObject);
        }
        // Otherwise if an instance already exists and it's not this one
        else
        {
            // Destroy this GameManager
            Destroy(gameObject);
        }
        source = GetComponent<AudioSource>();

        #endregion
    }

    public void TogglePause()
    {
        if (CurrentGameState == GameState.Playing)
        {
            PauseGame();
        }
        else if (CurrentGameState == GameState.Paused)
        {
            ResumeGame();
        }
    }

    public void PauseGame()
    {
        if (CurrentGameState == GameState.Paused) return;

        CurrentGameState = GameState.Paused;

        Time.timeScale = 0.0f;

        OnGamePaused.Invoke();
    }

    public void ResumeGame()
    {
        if (CurrentGameState == GameState.Playing) return;

        CurrentGameState = GameState.Playing;

        Time.timeScale = 1.0f;

        OnGameResumed.Invoke();
    }
    public void DeadGame()
    {
        if (lives <= 0)
        {
            if (CurrentGameState == GameState.Paused) return;

            CurrentGameState = GameState.Paused;

            Time.timeScale = 0.0f;

            OnGameDead.Invoke();
            source.PlayOneShot(clipDead);


        }
    }

    public void WonGame()
    {
        if (won)
        {
            if (CurrentGameState == GameState.Paused) return;

             CurrentGameState = GameState.Paused;

             Time.timeScale = 0.0f;

             OnGameWin.Invoke();
            source.PlayOneShot(clipWin);
        }
            
        
    }

    public void AddLives(int amount)
    {
        lives += amount;
    }

    public void SubtractLives(int amount)
    {
        lives -= amount;

        // You can include game over logic here if lives reach zero.
        if (lives <= 0)
        {
            // Handle game over, e.g., load a game over screen.
        }
    }
    public void ResetLives()
    {
        lives = 3;

    }

}