using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSoundEfect : MonoBehaviour
{
    public AudioSource source;
    [SerializeField] private AudioClip clip;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._instance.lives <= 0)
        {
            source.PlayOneShot(clip);
            
        }
    }
}
