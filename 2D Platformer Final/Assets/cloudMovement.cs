using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudMovement : MonoBehaviour
{
    [SerializeField] private float speed = 2.0f;  // Speed at which the cloud moves
    private float startX = 80.0f; // Starting X position
    private float endX = -12.0f; // Ending X position

    private Vector2 initialPosition;
    private Vector2 endPosition;

    private void Start()
    {
        initialPosition = transform.position;
        endPosition = new Vector2(endX, initialPosition.y);
    }

    private void Update()
    {
        //move the cloud to the right
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        //check if the cloud has reached the end position
        if (transform.position.x >= startX)
        {
            // reset the cloud's position to the end position
            transform.position = endPosition;
        }
    }
}

