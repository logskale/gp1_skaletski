//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[CreateAssetMenu(menuName = "InteractBehaviours/Sequence", fileName = "SequenceIBSO", order = 2)]
//public class SequenceIBSO : InteractBehaviourSO
//{
//    // A list of the behaviours to execute in sequence
//    [SerializeField] private List<InteractBehaviourSO> behaviours = new List<InteractBehaviourSO>();
    
//    // The behaviour we will execute next
//    private int behaviourIndex = 0;

//    public override void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
//    {
//        // Don't interact if there are no defined behaviours
//        if (behaviours.Count == 0)
//        {
//            Debug.LogWarning("No behaviours set in SequenceIBSO!");
//            return;
//        }

//        // Interact based on the behaviour of the current behaviour in the sequence
//        behaviours[behaviourIndex].Interact(playerInteractManager, playerBody);
//        behaviourIndex = (behaviourIndex + 1) % behaviours.Count;
//    }
//}
