//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[CreateAssetMenu(menuName = "InteractBehaviours/RandomSound", fileName = "RandomSoundIBSO", order = 1)]
//public class RandomSoundIBSO : InteractBehaviourSO
//{
//    // The audioclips we choose to play from at random
//    [SerializeField] private List<AudioClip> randomAudioClipList = new List<AudioClip>();

//    public override void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
//    {
//        // Do not play a sound if there aren't any to play
//        if (randomAudioClipList.Count == 0)
//        {
//            Debug.LogWarning("No audio clips given in RandomSoundIBSO!");
//            return;
//        }

//        // When interacted with, play a random sound from a list of sounds
//        int randomIndex = Random.Range(0, randomAudioClipList.Count);

//        // Use the player's AudioSource to play the random clip
//        playerBody.GetComponent<AudioSource>().PlayOneShot(randomAudioClipList[randomIndex]);
//    }
//}
