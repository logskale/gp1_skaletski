using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SnapshotSwitchTrigger : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private AudioMixerSnapshot pausedSnapshot;
    [SerializeField] private AudioMixerSnapshot unpausedSnapshot;

    private void SwitchAudioMixerSnapshot(bool pause)
    {
        AudioMixerSnapshot[] pauseSnapshots = new AudioMixerSnapshot[1];
        if (pause)
        {
         pauseSnapshots[0]=pausedSnapshot;
        }
        else
        {
            pauseSnapshots[0] = unpausedSnapshot;
        }
        float[] snapshotWeights = new float[1];
        snapshotWeights[0] = 1f;

        audioMixer.TransitionToSnapshots(pauseSnapshots, snapshotWeights, 1f);
       
    }

}
