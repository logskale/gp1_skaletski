using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VFXPropertyExample : MonoBehaviour
{

    [SerializeField] private VisualEffect visualeffect;
    // Start is called before the first frame update
    void Start()
    {
        //visualeffect.Play();
        //visualeffect.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        visualeffect.SetFloat("Lifetime",(Mathf.Sin(Time.time*.25f)*2)+3);
    }
}
