using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInteractable : MonoBehaviour, IInteractable
{
    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    { 
        //stop tracking the object
        playerInteractManager.StopTrackingObject(gameObject);
        //destroy the game object this script is on
        Destroy(gameObject);
    }
}
