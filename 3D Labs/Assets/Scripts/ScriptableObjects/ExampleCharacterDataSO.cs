using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is an example of a ScriptableObject asset that
/// both stores data and defines functionality.
/// </summary>

[CreateAssetMenu(fileName = "ExampleCharacterData", menuName = "Character/CharacterData", order = 0)]
public class ExampleCharacterDataSO : ScriptableObject
{
    // The data for this character
    public string characterName = "DefaultName";
    public int numLives = 3;

    // Used to initialize an instance of this type
    public void Init(string name, int lives)
    {
        characterName = name;
        numLives = lives;
    }

    // We can also initialize based off of another ExampleCharacterDataSO directly
    // This is similar to a copy-constructor
    public void Init(ExampleCharacterDataSO ecdso)
    {
        characterName = ecdso.characterName;
        numLives = ecdso.numLives;
    }

    // Functionality can also be defined in ScriptableObjects
    public void TakeDamage()
    {
        numLives -= 1;
    }

    // They can also be marked virtual/abstract for function overriding in child types
    public virtual void PrintCharacterData()
    {
        Debug.Log($"Character's name is {characterName} and they have {numLives} lives.");
    }
}
