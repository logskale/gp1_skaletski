using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractBehaviourSO : ScriptableObject
{
    // This function will be overridden and the implementation will determine the interact behaviour
    public abstract void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody);
}
