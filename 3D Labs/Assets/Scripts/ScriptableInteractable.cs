using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableInteractable : MonoBehaviour, IInteractable
{
    // The ScriptableObject that determines the behaviour to execute when interacted with
    [SerializeField] private InteractBehaviourSO ibso;

    public void Interact(PlayerInteractManager playerInteractManager, PlayerBody playerBody)
    {
        // Do whatever the IBSO says should happen when interacted with
        ibso.Interact(playerInteractManager, playerBody);
    }
}
