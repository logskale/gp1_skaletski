using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyBody : CharacterBody
{
    #region Variables

    [SerializeField] private bool aiActive = true; // Is the AI currently operating
    [SerializeField] private float pathingDelay = 0.1f; // Time to wait between path calculation attempts
    [SerializeField] private Transform targetTransform; // The target to move towards
    [SerializeField] private NavMeshAgent navMeshAgent; // The NavMeshAgent we use to calculate pathing (but not for movement)

    private Vector3 currentDestination; // The position we're pathing towards
    IEnumerator navigationCoroutine;
    Vector3[] debugPath; // Used to draw the navigation path

    #endregion

    #region Unity Functions

    private void Start()
    {
        // Tell the NavMeshAgent to not move our enemy
        navMeshAgent.isStopped = true;
        navMeshAgent.enabled = false;

        // Start manual navigation
        navigationCoroutine = EnemyNavigation();
        StartCoroutine(navigationCoroutine);
    }

    private void Update()
    {
        // Always move towards our target
        currentDestination = targetTransform.position;

        // Rotate the character's mesh
        RotateCharacter();
    }

    private void FixedUpdate()
    {
        // Move the character
        MoveCharacter();
    }

    #endregion

    #region Custom Functions

    #region Input

    public override void SetMoveInput(Vector2 moveInput)
    {
        movementDirection = moveInput;
    }
    #endregion

    #region Movement

    protected override void MoveCharacter()
    {
        // Move in movementDirection using the kinematic rigidbody
        Vector3 newPosition = transform.position + (movementDirection * (moveSpeed * Time.deltaTime));
        characterRigidbody.MovePosition(newPosition);
    }

    protected override void RotateCharacter()
    {
        // If we're trying to move
        if (movementDirection != Vector3.zero)
        {
            // Rotate our enemy mesh (with no vertical offset)
            Vector3 rotateDirection = movementDirection.normalized;
            rotateDirection.y = 0f;
            characterModel.forward = Vector3.Slerp(characterModel.forward, rotateDirection, rotationSpeed * Time.deltaTime);
        }
    }

    #endregion

    #region Navigation

    private IEnumerator EnemyNavigation()
    {
        // Wait for pathCalculationDelay seconds before trying to recalculate pathing
        WaitForSeconds WaitForPathingDelay = new WaitForSeconds(pathingDelay);

        // Only pathfind while the AI is active
        while (aiActive)
        {
            // Find next point to path to
            Vector3 nextPoint = GetDirectionToNextPathPoint();
            SetMoveInput(nextPoint); // If nextPoint is Vector3.zero, this doesn't change
            if (nextPoint != Vector3.zero)
            {
                Vector3 dirToNextPoint = nextPoint - transform.position;
                SetMoveInput(dirToNextPoint.normalized);
            }

            // Pathing delay
            yield return WaitForPathingDelay;
        }

        // We're done navigating, so stop moving
        SetMoveInput(Vector3.zero);
    }

    // Returns the next point in the path to our currentDestination
    private Vector3 GetDirectionToNextPathPoint()
    {
        // Calculate the path using the NavMeshAgent
        NavMeshPath path = new NavMeshPath();
        navMeshAgent.enabled = true;
        navMeshAgent.CalculatePath(currentDestination, path);
        navMeshAgent.enabled = false;

        // If we found a path with at least 2 points (a line)
        if (path != null && path.corners.Length >= 2)
        {
            debugPath = path.corners;
            // path.corners[0] is the start of the path (where we currently are), so get the second point instead
            return path.corners[1];
        }
        else // Did not find a path, or already at the destination
        {
            debugPath = null;
            return Vector3.zero;
        }
    }

    #endregion

    #endregion

    // Debug drawing of the calculated navigation path
    private void OnDrawGizmos()
    {
        if (debugPath != null && debugPath.Length >= 2)
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < debugPath.Length - 1; i++)
            {
                Gizmos.DrawWireSphere(debugPath[i], 0.1f); Gizmos.DrawLine(debugPath[i], debugPath[i + 1]);
            }

            Gizmos.color = Color.magenta; Gizmos.DrawSphere(debugPath[debugPath.Length - 1], 0.1f);
            Gizmos.color = Color.green; Gizmos.DrawSphere(debugPath[1], 0.1f);
        }
    }
}