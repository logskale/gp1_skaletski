using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PlugableAI/Actions/GoHome", fileName = "A_GoHome")]
public class GoHomeAction : Action
{
    public override void Act(Blackboard blackboard)
    {
        GoHome(blackboard);  
    }

    public void GoHome(Blackboard blackboard)
    {
        //the GoHome action assumes we have a NavMeshAgent
        blackboard.navMeshAgent.SetDestination(blackboard.homeWaypoint.position);
        blackboard.navMeshAgent.isStopped = false;
    }
}
