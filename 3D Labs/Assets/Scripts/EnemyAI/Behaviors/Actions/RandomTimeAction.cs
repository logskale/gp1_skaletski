using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlugableAI/Actions/RandomNumber", fileName = "A_RandomNumber")]
public class RandomTimeAction : Action
{
    public float minTime = 2f;
    public float maxTime = 5f;

    public override void Act(Blackboard blackboard)
    {
        RandomNumber(blackboard);
    }

    public void RandomNumber(Blackboard blackboard)
    {

        blackboard.randomNumber= Random.Range(minTime, maxTime);
    }
}
