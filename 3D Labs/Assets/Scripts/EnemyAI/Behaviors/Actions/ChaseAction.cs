using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ChaseAction makes the AI agent move towards the chaseTarget (the player).
/// </summary>

[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase", fileName = "A_Chase")]
public class ChaseAction : Action
{
    public override void Act(Blackboard blackboard)
    {
        Chase(blackboard);
    }

    private void Chase(Blackboard blackboard)
    {
        // Do nothing if the chaseTarget is null
        if (blackboard.chaseTarget == null) return;

        blackboard.navMeshAgent.destination = blackboard.chaseTarget.position;
        blackboard.navMeshAgent.isStopped = false;
    }
}