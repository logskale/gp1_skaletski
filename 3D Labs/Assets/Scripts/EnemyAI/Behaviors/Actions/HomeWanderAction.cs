using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="PlugableAI/Actions/HomeWander", fileName ="A_HomeWander")]
public class HomeWanderAction : Action
{
    public override void Act(Blackboard blackboard)
    {
        NavigateToRandomPoint(blackboard);
    }
    private void NavigateToRandomPoint (Blackboard blackboard)
    {
        Vector2 randomOffset = Random.insideUnitCircle;
        Vector3 randomXZ = new Vector3(randomOffset.x, 0f, randomOffset.y);
        Vector3 randomPointInWanderRadius = blackboard.homeWaypoint.position + (randomXZ * blackboard.aiStats.maxDistanceFromHome);

        blackboard.navMeshAgent.SetDestination(randomPointInWanderRadius);
        blackboard.navMeshAgent.isStopped = false;
    }
}
