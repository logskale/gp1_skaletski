using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Decision/Selector", fileName = "D_SelectorDecision")]
public class SelectorDecision : Decision
{
    public Decision[] decisions;

    public override bool Decide(Blackboard blackboard)
    {
        foreach (Decision decision in decisions)
        {
            if (decision.Decide(blackboard))
            {
                return true;
            }
        }
        return false;
    }
}