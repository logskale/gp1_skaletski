using UnityEngine;

[CreateAssetMenu(menuName = "Decision/RandomTimeElapsed", fileName = "D_RandomTimeElapsedDecision")]
public class RandomTimeElapsedDecision : Decision
{


    public override bool Decide(Blackboard blackboard)
    {
        float elapsedTime = blackboard.owningController.stateTimeElapsed;
        
        return elapsedTime >= blackboard.randomNumber;
    }
    
}
