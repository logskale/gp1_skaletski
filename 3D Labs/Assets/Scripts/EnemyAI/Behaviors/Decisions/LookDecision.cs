using UnityEngine;

/// <summary>
/// The LookDecision evaluates to true if the player
/// is detected within the SphereCast/OverlapSphere
/// based on the AIStats parameters and false otherwise.
/// </summary>

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Look", fileName = "D_Look")]
public class LookDecision : Decision
{
    // the layer mask that the player is in
    [SerializeField] private LayerMask characterLayerMask;

    // call Look to see if we find the player and return true if so, false otherwise
    public override bool Decide(Blackboard blackboard)
    {
        bool targetVisible = Look(blackboard);

        return targetVisible;
    }

    // we can draw
    private bool Look(Blackboard blackboard)
    {
        Debug.DrawRay(blackboard.eyes.position, blackboard.eyes.forward.normalized * blackboard.aiStats.lookRange, Color.green);

        // If our spherecast hit something and the thing that it hit is the player
        RaycastHit hit;
        Collider[] cols;
        if (Physics.SphereCast(blackboard.eyes.position,
                blackboard.aiStats.lookSphereCastRadius,
                blackboard.eyes.forward,
                out hit,
                blackboard.aiStats.lookRange,
                characterLayerMask,
                QueryTriggerInteraction.Ignore)
           && hit.collider.CompareTag("Player"))
        {
            blackboard.chaseTarget = hit.transform;
            return true;
        }
        // Do a secondary check with an OverlapSphere to check the initial position of the spherecast.
        // Note: SphereCast will not detect colliders for which the sphere initially overlaps the collider.
        else if ((cols = Physics.OverlapSphere(blackboard.eyes.position, blackboard.aiStats.lookSphereCastRadius, characterLayerMask, QueryTriggerInteraction.Ignore)) != null)
        {
            // For each overlapped collider
            foreach (Collider collider in cols)
            {
                // If this is the Player
                if (collider.CompareTag("Player"))
                {
                    blackboard.chaseTarget = hit.transform;
                    return true;
                }
            }
        }

        // if we get here, we didn't find the player!
        return false;
    }
}