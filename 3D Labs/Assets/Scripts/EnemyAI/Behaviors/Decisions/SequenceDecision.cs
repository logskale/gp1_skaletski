using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Decision/Sequence",fileName = "D_SequenceDecision")]
public class SequenceDecision : Decision
{
    public Decision[] decisions;

    public override bool Decide(Blackboard blackboard)
    {
        foreach (Decision decision in decisions)
        {
            if (!decision.Decide(blackboard))
            {
                return false;
            }
        }
        return true;
    }
}