using UnityEngine;

/// <summary>
/// The ScanDecision evaluates to true if the scan
/// completed uninterrupted, and otherwise false
/// if it was interrupted.
/// </summary>

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Scan", fileName = "D_Scan")]
public class ScanDecision : Decision
{
    public override bool Decide(Blackboard blackboard)
    {
        bool noEnemyInSight = Scan(blackboard);

        return noEnemyInSight;
    }

    private bool Scan(Blackboard blackboard)
    {
        // Return true if we've been in this state for searchDuration seconds, and false otherwise
        return blackboard.owningController.CheckIfStateTimeElapsed(blackboard.aiStats.searchDuration);
    }
}