using UnityEngine;

/// <summary>
/// The State class contains lists of Actions to be performed at various points of State
/// Transitions, as well as Transitions out of the State into other States.
/// </summary>

[CreateAssetMenu(menuName = "FSM/State", fileName = "State")]
public class State : ScriptableObject
{
    // First, we have lists of enter, exit, and update actions
    [Tooltip("A list of all Actions to execute when this state is entered.")]
    [SerializeField] private Action[] enterActions;
    [Tooltip("A list of all Actions to execute when this state is exited.")]
    [SerializeField] private Action[] exitActions;
    [Tooltip("A list of all Actions to execute when this state is called in the Update() loop.")]
    [SerializeField] private Action[] updateActions;

    // Next we have our transitions, which are defined by Conditions
    [Tooltip("A list of all possible Transitions out of this State")]
    [SerializeField] private Transition[] transitions;

    [Tooltip("A list of all possible Transitions out of this State")]
    public float randomTime;

    // Now define our enter, exit, and update state functions
    public void EnterState(Blackboard blackboard)
    {
        // Perform the enter actions of this state
        DoEnterActions(blackboard);
    }

    public void ExitState(Blackboard blackboard)
    {
        // Perform the exit actions of this state
        DoExitActions(blackboard);
    }

    public void UpdateState(Blackboard blackboard)
    {
        // Perform the update actions of this state
        DoUpdateActions(blackboard);

        // Check our transitions to see if we should change state
        CheckTransitions(blackboard);
    }

    private void DoEnterActions(Blackboard blackboard)
    {
        // For every action of this state
        foreach (Action enterAction in enterActions)
        {
            // Perform the action
            enterAction.Act(blackboard);
        }
    }

    private void DoExitActions(Blackboard blackboard)
    {
        // For every action of this state
        foreach (Action exitAction in exitActions)
        {
            // Perform the action
            exitAction.Act(blackboard);
        }
    }

    private void DoUpdateActions(Blackboard blackboard)
    {
        // For every action of this state
        foreach (Action updateAction in updateActions)
        {
            // Perform the action
            updateAction.Act(blackboard);
        }
    }

    private void CheckTransitions(Blackboard blackboard)
    {
        // For every transition this state contains
        foreach (Transition transition in transitions)
        {
            // Evaluate the decision of the transition
            bool decisionSucceeded = transition.decision.Decide(blackboard);

            // If the decision evaluated to true
            if (decisionSucceeded)
            {
                // Attempt to transition to the next state
                bool transitionSucceeded= blackboard.owningController.TransitionToState(transition.nextState);

                // Now stop evaluating our transitions if a decision succeeded
                if (transitionSucceeded)break;
            }
        }
    }
}