using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleCharacter : MonoBehaviour
{
    // The ScriptableObject data asset from which we copy our data
    [SerializeField] private ExampleCharacterDataSO characterDataSO;
    
    // The instance of the ScriptableObejct data asset
    private ExampleCharacterDataSO cdsoInstance;

    private void Start()
    {
        // Make an instance of our ScriptableObject data asset so we don't modify the original
        cdsoInstance = ScriptableObject.CreateInstance<ExampleCharacterDataSO>();
        cdsoInstance.Init(characterDataSO.characterName, characterDataSO.numLives);

        // We can call functions from ScriptableObjects too!
        cdsoInstance.PrintCharacterData();
        cdsoInstance.TakeDamage();
        cdsoInstance.PrintCharacterData();
    }
}
