﻿//By Sam Belin
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class GameControl : MonoBehaviour {
    //The game control script is useful for keeping track of items in a central location
    //In this case the score and the timer are displayed

	//public bool levelStart=false;

    //make a variable to hold the instance of the Scorer class
	// public Scorer score;

    //make a variable to hold the instance of the Countdown script
	// private Countdown cdTimer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.M)){
            SceneManager.LoadScene("EndGame", LoadSceneMode.Single);
        }
	}

}
