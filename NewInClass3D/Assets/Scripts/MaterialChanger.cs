using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChanger : MonoBehaviour
{
    [SerializeField] private Material material;

        private int tintColorID;

    private Color tintColor;
    private void Awake()
    {
        tintColorID = Shader.PropertyToID("_Tint_Color");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tintColor = new Color(1f, Time.time%1f,1f);
        material.SetColor(tintColorID, tintColor);
    }
}
