using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    //set a horizontal and vertical speed
	public float hSpeed = 6f;
    public float vForce = 1f;
    public float maxSpeed = 6f;
	public LayerMask boxMask;
    private bool jump = false;
    private bool isJumping = false;
	//on something in the Ground layer
	private bool onGround = false;
    private bool onGrabable = false;
    private bool onGroundBack = false;
    private bool onGrabableBack = false;
	public Transform groundCheckGizmo;
    public Transform groundCheckGizmoBack;
    private string currentSceneName;
    private enum plyrDirection { Left, Idle, Right, Jump };
	//This creates a variable to track the states
	private plyrDirection playerState;

	//Makes a variable for the animator so we can access the parameters within it.
	private Animator anim;


	public SpriteRenderer spriteRenderer;
	public Sprite[] spriteArray;



	// Use this for initialization
	void Start () {
        //getting current scence the player is in
        currentSceneName = SceneManager.GetActiveScene().name;
        //Set the initial state to Idle
		playerState = plyrDirection.Idle;
		//Get the animator component so we can access it through our script
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis("Horizontal");
		anim.SetFloat("speed", Mathf.Abs(h));
		ChangePlayerModel();

		onGround = Physics2D.Linecast(transform.position, groundCheckGizmo.position, 1<<LayerMask.NameToLayer("Ground"));
        onGrabable = Physics2D.Linecast(transform.position, groundCheckGizmo.position, 1<<LayerMask.NameToLayer("Grabable"));
        onGroundBack = Physics2D.Linecast(transform.position, groundCheckGizmoBack.position, 1<<LayerMask.NameToLayer("Ground"));
        onGrabableBack = Physics2D.Linecast(transform.position, groundCheckGizmoBack.position, 1<<LayerMask.NameToLayer("Grabable"));

        if(Input.GetKeyDown(KeyCode.R)){
            SceneManager.LoadScene(currentSceneName, LoadSceneMode.Single);
        }

        if (h>0){
			playerState=plyrDirection.Right;
			ChangeDirections();
		}else if(h<0){
			playerState=plyrDirection.Left;
			ChangeDirections();
		}else if(h==0){
			playerState=plyrDirection.Idle;
		}

        if(Input.GetKeyUp("up") || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.Space)){
            isJumping = false;
        }
        if (Input.GetButton("Jump") && (onGround || onGrabable) && !isJumping){
			jump=true;
		}else if(Input.GetButton("Jump") && (onGroundBack || onGrabableBack) && !isJumping){
            jump = true;
        }
	}

    void FixedUpdate(){
        float h = Input.GetAxis("Horizontal");
        if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x)<=maxSpeed){
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * hSpeed, ForceMode2D.Impulse);
        }

		if(jump){
			GetComponent<Rigidbody2D>().AddForce(new Vector2(0, vForce), ForceMode2D.Impulse);
            jump=false;
            isJumping = true;
		}
    }

    void ChangeDirections(){
		Vector3 playerScale = new Vector3(0.3f,0.3f,0.3f);
		if(playerState == plyrDirection.Right){
			playerScale = new Vector3(0.3f,0.3f,0.3f);
		}else if(playerState == plyrDirection.Left){
			playerScale = new Vector3(-0.3f,0.3f,0.3f);
		}

		transform.localScale = playerScale;
	}

	void ChangePlayerModel(){
		if(Input.GetKeyDown(KeyCode.J)){
			spriteRenderer.sprite = spriteArray[0]; 
		}else if(Input.GetKeyDown(KeyCode.K)){
			spriteRenderer.sprite = spriteArray[1]; 
		}else if(Input.GetKeyDown(KeyCode.L)){
			spriteRenderer.sprite = spriteArray[2]; 
		}
	}

}

