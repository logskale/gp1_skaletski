﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    //create a variable to hold the transform of the player
	public Transform player;
	
	// Update is called once per frame
	void LateUpdate () {
        //Late Update is called after the Updates on all objects
        //By putting the camera movement in the LateUpdate we can ensure that the player movement has happened
        //Grab the current y position of the camera
        //Since the only movement of the player in y is a jump we will just follow on x
		float yPos = player.transform.position.y;

        //Grab the current x position of the player
		float xPos = player.transform.position.x;

		//Grab the current z position of the camera
		float zPos = transform.position.z;

        //Move the camera, which this script has been applied to, to the location specified above
		transform.position =new Vector3(xPos,yPos,zPos);
	}
}
